#include"Regle.h"

/*REGLE NB VOISINS*/

bool Regle_Nb_Voisins::declenchement(Etat Ea, std::vector<Etat> voisinage) {
	int i;
	int nV = 0;
	for (i = 0; i < voisinage.size(); i++) {
		if (voisinage[i] == Etat_Voisin)
			nV++;
	}
	if (Ea == Etat_Actuel) {
		if (operateur == '=')
			return(nV == nb_Voisin);
        if (operateur == '>')
			return(nb_Voisin < nV);
        if (operateur == '<')
			return(nb_Voisin > nV);
	}
	return(false);
}

/*REGLE AUTOMATIQUES*/

bool Regle_Auto::declenchement(Etat Ea, std::vector<Etat> V) {
	if (Ea == Etat_Actuel) {
		return(true);
	}
	return(false);
}

/*REGLES MOTIF*/

bool Regle_Motif::declenchement(Etat Ea, std::vector<Etat> V) {
	bool test;
	int j;
	if ((Ea == Etat_Actuel)) {
		test = true;
		for (j = 0; j < V.size(); j++) {
            if (V[j] != motif[j])
            {
				test = false;
                break;
            }
		}
		if (test) {
			return(true);
		}
	}
	return false;
}

/*REGLE MOTIF SOUPLE*/
bool Regle_Motif_Souple::declenchement(Etat Ea, std::vector<Etat> V) {
    bool test;
    int j;
    if ((Ea == Etat_Actuel)) {
        test = true;
        for (j = 0; j < V.size(); j++) {
            if (V[j] != motif[j] && motif[j].getIndice() != -1)
            {
                test = false;
                break;
            }
        }
        if (test) {
            return(true);
        }
    }
    return false;
}

/*REGLE MOTIF LANGTON LOOP*/

bool Regle_Motif_Langton_Loop::declenchement(Etat Ea, std::vector<Etat> V) {
	bool test;
	int i,j;
	if ((Ea == Etat_Actuel)) {
		for (i = 0; i < V.size(); i++) {
			test = true;
			for (j = 0; j < V.size(); j++) {
				if (V[(i + j) % V.size()] != motif[j])
					test = false;
			}
			if (test)
			{
				return(true);
			}			
		}
	}
	return false;
}
