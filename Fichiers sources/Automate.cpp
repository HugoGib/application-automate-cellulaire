#include"Automate.h"

/*AUTOMATE*/



void Automate::Transition(Reseau& R) {
	std::vector<Etat> voisinage;
	Reseau sauvegarde_Reseau = Reseau(R);
	for (int x = 0; x < R.getLargeur(); x++) {
		for (int y = 0; y < R.getHauteur(); y++) {
			voisinage = typeVoisinage->voisins(sauvegarde_Reseau, x, y);
			for (unsigned int i = 0; i < regles.size(); i++) {
                Etat cellule = sauvegarde_Reseau.getCellule(x, y);
                if (regles[i]->declenchement(cellule, voisinage))
				{
					R.setCellule(regles[i]->application(), x, y);
					break;
				}
			}
		}
	}
}

Reseau Automate::initialisation(unsigned int l, unsigned int h) {
    Reseau reseau = Reseau(l, h);
    unsigned int e, i, j;
    for (i = 0; i < l; i++) {
        for (j = 0; j < h; j++) {
            e = std::rand() % (alphabet.size());
            reseau.setCellule(alphabet[e], i, j);
        }
    }
    return(reseau);
}

/*AUTOMATE LIFE'S GAME*/

Automate_Life_Game::Automate_Life_Game() : Automate(new Voisinage_Moore(1)) {
	Etat Mort = Etat(0, 0, 0, 0, "Mort");
	alphabet.push_back(Mort);
	Etat Vivant = Etat(255, 255, 255, 1, "Vivant");
	alphabet.push_back(Vivant);
    Regle_Nb_Voisins* regle = new Regle_Nb_Voisins(Mort, Vivant, Vivant, '=', 3);
    regles.push_back(regle);
    regle = new Regle_Nb_Voisins(Vivant, Vivant, Mort, '>', 3);
    regles.push_back(regle);
    regle = new Regle_Nb_Voisins(Vivant, Vivant, Mort, '<', 2);
    regles.push_back(regle);
}



/*AUTOMATE BRIAN'S BRAIN*/

Automate_Brian_Brain::Automate_Brian_Brain() : Automate(new Voisinage_Moore(1)) {
    Etat Repos = Etat(0, 0, 0, 0, "Repos");
	alphabet.push_back(Repos);
    Etat Excite = Etat(0, 0, 255, 1, "Excite");
	alphabet.push_back(Excite);
    Etat Refractaire = Etat(0, 200, 180, 2, "Refractaire");
	alphabet.push_back(Refractaire);

	Regle_Auto* regleAuto = new Regle_Auto(Excite, Refractaire);
    regles.push_back(dynamic_cast<Regle*>(regleAuto));
	regleAuto = new Regle_Auto(Refractaire, Repos);
	regles.push_back(regleAuto);
	Regle_Nb_Voisins* regleNbVoisins;
	regleNbVoisins = new Regle_Nb_Voisins(Repos, Excite, Excite, '=', 2);
	regles.push_back(regleNbVoisins);
}



/*AUTOMATE GRIFFEATH*/

Automate_Griffeath::Automate_Griffeath() : Automate(new Voisinage_Moore(1)) {
	Etat Jaune = Etat(255, 255, 0, 0, "Jaune");
	alphabet.push_back(Jaune);
	Etat Orange_clair = Etat(255, 200, 0, 1, "Orange clair");
	alphabet.push_back(Orange_clair);
	Etat Orange_fonce = Etat(255, 100, 0, 2, "Orange fonce");
	alphabet.push_back(Orange_fonce);
	Etat Rouge = Etat(255, 0, 0, 3, "Rouge");
	alphabet.push_back(Rouge);

	Regle_Nb_Voisins* regle;
    regle = new Regle_Nb_Voisins(Jaune, Orange_clair, Orange_clair, '>', 2);
	regles.push_back(regle);
    regle = new Regle_Nb_Voisins(Orange_clair, Orange_fonce, Orange_fonce, '>', 2);
	regles.push_back(regle);
    regle = new Regle_Nb_Voisins(Orange_fonce, Rouge, Rouge, '>', 2);
	regles.push_back(regle);
    regle = new Regle_Nb_Voisins(Rouge, Jaune, Jaune, '>', 2);
	regles.push_back(regle);
}

/*AUTOMATE FOURMI LANGTON*/
Automate_Fourmi_Langton::Automate_Fourmi_Langton() : Automate(new Voisinage_Neumann(1)) {
    Etat etat0 = Etat(255,255,255,0,"blanc");
    Etat etat1 = Etat(0,0,0,1,"noir");
    Etat etat2 = Etat(190,190,255,2,"blanc haut");
    Etat etat3 = Etat(65,65,0,3,"noir haut");
    Etat etat4 = Etat(255,190,190,4,"blanc gauche");
    Etat etat5 = Etat(0,65,65,5,"noir gauche");
    Etat etat6 = Etat(190,255,190,6,"blanc bas");
    Etat etat7 = Etat(65,0,65,7,"noir bas");
    Etat etat8 = Etat(190,190,190,8,"blanc droite");
    Etat etat9 = Etat(65,65,65,9,"noir droite");
    alphabet = {etat0,etat1,etat2,etat3,etat4,etat5,etat6,etat7,etat8,etat9};

    std::vector<Etat> motif = {etat0,etat0,etat0,etat0};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat0, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat1, motif));

    regles.push_back(new Regle_Auto(etat3,etat0));
    regles.push_back(new Regle_Auto(etat5,etat0));
    regles.push_back(new Regle_Auto(etat7,etat0));
    regles.push_back(new Regle_Auto(etat9,etat0));

    regles.push_back(new Regle_Auto(etat2,etat1));
    regles.push_back(new Regle_Auto(etat4,etat1));
    regles.push_back(new Regle_Auto(etat6,etat1));
    regles.push_back(new Regle_Auto(etat8,etat1));

    Etat etat10 = Etat(0,0,0,-1,"wildcard");

    motif = {etat10,etat2,etat10,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat8, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat9, motif));

    motif = {etat10,etat10,etat3,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat4, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat5, motif));

    motif = {etat10,etat10,etat10,etat4};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat2, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat3, motif));

    motif = {etat5,etat10,etat10,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat6, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat7, motif));

    motif = {etat10,etat10,etat6,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat4, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat5, motif));

    motif = {etat10,etat7,etat10,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat8, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat9, motif));


    motif = {etat8,etat10,etat10,etat10};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat6, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat7, motif));

    motif = {etat10,etat10,etat10,etat9};
    regles.push_back(new Regle_Motif_Souple(etat0 , etat2, motif));
    regles.push_back(new Regle_Motif_Souple(etat1 , etat3, motif));

}

Reseau Automate_Fourmi_Langton::initialisation(unsigned int l, unsigned int h) {
    Reseau reseau = Reseau(alphabet[0], l, h);
    unsigned int x = static_cast<unsigned int>(l / 2); // x et y sont l'index du centre de la premiere boucle de langton
    unsigned int y = static_cast<unsigned int>(h / 2);
    reseau.setCellule(alphabet[2],x,y);
    return(reseau);
}

/*AUTOMATE LANGSTON'S LOOP*/

Automate_Langton_Loop::Automate_Langton_Loop() : Automate(new Voisinage_Langton_Loop()) {
	//Les 8 �tats de la boucle de Langston
	Etat Etat0 = Etat(0, 0, 0, 0, "Etat0");
	alphabet.push_back(Etat0);
	Etat Etat1 = Etat(0, 255, 0, 1, "Etat1");
	alphabet.push_back(Etat1);
	Etat Etat2 = Etat(255, 0, 0, 2, "Etat2");
	alphabet.push_back(Etat2);
	Etat Etat3 = Etat(255, 255, 0, 3, "Etat3");
	alphabet.push_back(Etat3);
	Etat Etat4 = Etat(255, 100, 0, 4, "Etat4");
	alphabet.push_back(Etat4);
	Etat Etat5 = Etat(255, 0, 255, 5, "Etat5");
	alphabet.push_back(Etat5);
	Etat Etat6 = Etat(255, 255, 255, 6, "Etat6");
	alphabet.push_back(Etat6);
	Etat Etat7 = Etat(0, 0, 255, 7, "Etat7");
	alphabet.push_back(Etat7);

    std::vector<Etat> regleMotif;
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0 , Etat0, regleMotif)); //Ici on cr�er un voisinage qui declanchera une Regle motif
	//puis on cr�er la r�gle

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat3, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));
	
	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat0, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat3, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));
	
	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat1, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat3, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat7, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat3, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat6);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat2, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat4);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat6);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat3, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat6, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat4, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat0, regleMotif));


	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat4);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat4, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat5, Etat2, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat3);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat6, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat1);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat0, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat3);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat1, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat5);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat5, regleMotif));

	regleMotif.clear();
	regleMotif.push_back(Etat0);
	regleMotif.push_back(Etat2);
	regleMotif.push_back(Etat7);
	regleMotif.push_back(Etat2);
	regles.push_back(new Regle_Motif_Langton_Loop(Etat7, Etat0, regleMotif));
}

Reseau Automate_Langton_Loop::initialisation(unsigned int l, unsigned int h) {
    if (l < 30) l = 30;
    if (h < 30) l = 30;
    Reseau reseau = Reseau(alphabet[0], l, h);
    unsigned int i;
    unsigned int x = static_cast<unsigned int>(l / 2); // x et y sont l'index du centre de la premiere boucle de langton
    unsigned int y = static_cast<unsigned int>(h / 2);
    //Membrane
    for (i = y-3; i <= y+4; i++) {
        reseau.setCellule(alphabet[2], x-4, i);
        reseau.setCellule(alphabet[2], x+5, i);
    }
    for (i = y+5; i <= y+9; i++) {
        reseau.setCellule(alphabet[2], x+3, i);
        reseau.setCellule(alphabet[2], x+5,i);
    }
    for (i = x-3; i <= x+3; i++) {
        reseau.setCellule(alphabet[2], i, y-4);
        reseau.setCellule(alphabet[2], i, y+5);
    }
    reseau.setCellule(alphabet[2], x+4, y-4);
    reseau.setCellule(alphabet[2], x+5, y-4);
    reseau.setCellule(alphabet[2], x+4, y+10);
    for (i = x-2; i <= x+3; i++) {
        reseau.setCellule(alphabet[2], i, y-2);
        reseau.setCellule(alphabet[2], i, y+3);
    }
    for(i = y-1; i<=y+2;i++){
        reseau.setCellule(alphabet[2], x-2, i);
        reseau.setCellule(alphabet[2], x+3, i);
    }
    //ADN
    reseau.setCellule(alphabet[1], x-3, y - 3);
    reseau.setCellule(alphabet[7], x-1, y - 3);
    reseau.setCellule(alphabet[1], x, y - 3);
    reseau.setCellule(alphabet[7], x+2, y - 3);
    reseau.setCellule(alphabet[1], x+3, y - 3);

    reseau.setCellule(alphabet[7], x-3, y - 2);
    reseau.setCellule(alphabet[1], x-3, y);
    reseau.setCellule(alphabet[4], x-3, y + 1);
    reseau.setCellule(alphabet[1], x-3, y + 3);
    reseau.setCellule(alphabet[4], x-3, y + 4);

    for (i = x-1; i <= x+3; i++)
        reseau.setCellule(alphabet[1], i, y + 4);

    reseau.setCellule(alphabet[7], x + 4, y - 2);
    reseau.setCellule(alphabet[1], x + 4, y - 1);
    reseau.setCellule(alphabet[7], x + 4, y + 1);
    reseau.setCellule(alphabet[1], x + 4, y + 2);
    reseau.setCellule(alphabet[7], x + 4, y + 4);

    for(i = y + 5; i<=y+9 ; i++)
        reseau.setCellule(alphabet[1], x + 4, i);

    return(reseau);
}
/*AUTOMATE UTILISATEUR*/

//Automate_Utilisateur::Automate_Utilisateur() : Automate(new Voisinage_Moore(1)) {
	
//}
