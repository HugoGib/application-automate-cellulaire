QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    Automate.h \
    Etat.h \
    FenetreAlphabet.h \
    FenetreAutomate.h \
    FenetreRegleMotif.h \
    FenetreRegles.h \
    FenetreReseau.h \
    FenetreSimulation.h \
    FenetreVoisinage.h \
    FenetreVoisinageArbitraire.h \
    Regle.h \
    Reseau.h \
    Simulateur.h \
    Voisinage.h


SOURCES += \
    Automate.cpp \
    Etat.cpp \
    FenetreAlphabet.cpp \
    FenetreAutomate.cpp \
    FenetreRegleMotif.cpp \
    FenetreRegles.cpp \
    FenetreReseau.cpp \
    FenetreSimulation.cpp \
    FenetreVoisinage.cpp \
    FenetreVoisinageArbitraire.cpp \
    Regle.cpp \
    Simulateur.cpp \
    Voisinage.cpp \
    main.cpp
