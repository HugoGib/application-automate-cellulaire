#include "FenetreSimulation.h"

FenetreSimulation::FenetreSimulation(Simulateur* sim, QWidget* parent):QWidget(parent), simulateur(sim), simulationEnCours(false),iteration(0),sauteIteration(0)
{
    int taille = 25;
    int hauteurRes = simulateur->getReseauInitial().getHauteur();
    int largeurRes=simulateur->getReseauInitial().getLargeur();


    res = new QTableWidget(hauteurRes,largeurRes,this);

    QHeaderView* header = res ->horizontalHeader();
    header->setMaximumSectionSize(15);

    header = res ->verticalHeader();
    header->setMaximumSectionSize(15);

    LabelLayout = new QVBoxLayout;
    LayoutH = new QHBoxLayout;

    ResLayout = new QVBoxLayout;
    ResLabel = new QLabel("Simulation");

     for (int i = 0; i<hauteurRes ; i++ ) {
        res->setColumnWidth(i,taille);
        res->setRowHeight(i,taille);
    }
    for(int i = 0; i < simulateur->getAutomate().getAlphabet().size();i++){
        QString str = QString::fromStdString(std::to_string(i)+" : "+simulateur->getAutomate().getAlphabet()[i].getLabel());
        QLabel* label = new QLabel(str);
        LabelLayout->addWidget(label);
    }

    for(unsigned int i=0; i<hauteurRes; i++)
    {
        for(unsigned int j=0; j<largeurRes; j++){
            int indice = simulateur->getReseauInitial().getCellule(i,j).getIndice();
            res->setItem(i, j, new QTableWidgetItem(""+indice));
            int r = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getR();
            int g = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getG();
            int b = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getB();
            res->item(i,j)->setBackground(QColor(r, g, b));
            res->item(i,j)->setForeground(QColor(r, g, b));
        }
    }

    res->horizontalHeader()->setVisible(false); //Pas de nom pour les colonnes.
    res->verticalHeader()->setVisible(false);
    boutonReseau = new QPushButton("Etape suivante",this);
    LabelLayout->addWidget(boutonReseau);
    QObject::connect(boutonReseau, SIGNAL(clicked()), this, SLOT(runSim()));

    boutonRun = new QPushButton("Run",this);
    LabelLayout->addWidget(boutonRun);
    QObject::connect(boutonRun, SIGNAL(clicked()), this, SLOT(changeSimulationState()));

    boutonReset = new QPushButton("Reset",this);
    LabelLayout->addWidget(boutonReset);
    QObject::connect(boutonReset, SIGNAL(clicked()), this, SLOT(reset()));

    VitesseLayout = new QVBoxLayout;

    sliderVitesse = new QSlider(Qt::Horizontal);
    sliderVitesse->setFixedWidth(100);
    sliderVitesse->setMinimum(1); // en étape par seconde
    sliderVitesse->setMaximum(1100);
    connect(sliderVitesse, SIGNAL(valueChanged(int)), this, SLOT(changerVitesse(int)));
    QLabel* labelSliderVitesse = new QLabel("Vitesse",this);
    labelVitesse = new QLabel(QString::number(sliderVitesse->value()),this);
    VitesseLayout->addWidget(labelSliderVitesse);
    VitesseLayout->addWidget(labelVitesse);
    VitesseLayout->addWidget(sliderVitesse);
    labelIteration = new QLabel(tr("Iteration ")+QString::number(iteration),this);
    VitesseLayout->addWidget(labelIteration);


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(runSimAuto()));
    timer->start(1000/sliderVitesse->value());


    LabelLayout->addLayout(VitesseLayout);

    LayoutH->addWidget(res);
    LayoutH->addLayout(LabelLayout);

    ResLayout->addWidget(ResLabel);
    ResLayout->addLayout(LayoutH);
    setLayout(ResLayout);
}

void FenetreSimulation::changerVitesse(int nouvell_vitesse){
    timer->setInterval(1000/nouvell_vitesse);
    labelVitesse->setText(QString::number(sliderVitesse->value()));
    if(nouvell_vitesse>100){
        timer->setInterval(0);
        sauteIteration = nouvell_vitesse-100;
    }
    else
    {
        sauteIteration = 0;
    }
}

void FenetreSimulation::runSimAuto(){
    if(simulationEnCours)
    {
        runSim();
    }
}


void FenetreSimulation::runSim(){
    simulateur->run();
    if(sauteIteration == 0 || (iteration%sauteIteration)==0)
    actualiserTable();
    iteration++;
    labelIteration->setText(tr("Iteration ")+QString::number(iteration));
}

void FenetreSimulation::actualiserTable()
{
    int hauteurRes = simulateur->getReseauInitial().getHauteur();
    int largeurRes=simulateur->getReseauInitial().getLargeur();
    for(unsigned int i=0; i<hauteurRes; i++)
    {
        for(unsigned int j=0; j<largeurRes; j++){
            int indice = simulateur->getReseau().getCellule(i,j).getIndice();
            res->item(i, j)->setText(QString::number(indice));
            int r = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getR();
            int g = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getG();
            int b = simulateur->getAutomate().getAlphabet()[indice].getCouleur().getB();
            res->item(i,j)->setBackground(QColor(r, g, b));
            res->item(i,j)->setForeground(QColor(r, g, b));
        }
    }
}

void FenetreSimulation::changeSimulationState(){
    simulationEnCours = simulationEnCours ? false : true;
    if(simulationEnCours)
    {
        boutonRun->setText("Stop");
    }
    else
    {
        boutonRun->setText("Run");
    }
}

void FenetreSimulation::reset(){
    simulateur->reset();
    actualiserTable();
    iteration = 0;
}
