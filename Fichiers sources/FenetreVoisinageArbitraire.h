/**
 * \file          Fenetre_Initialisation_Automate.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un voisinage arbitraire pour l'automate utilisateur.
 *
 */
#ifndef FENETRE_VOISINAGE_ARBITRAIRE
#define FENETRE_VOISINAGE_ARBITRAIRE

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <vector>
#include <QVBoxLayout>
#include <QTableWidget>
#include "Etat.h"
#include "Voisinage.h"

 /**
  * \class FenetreVoisinageArbitraire
  * \brief	Classe pour creer une fenetre pour creer un voisinage arbitraire pour l'automate utilisateur.
 *
  *
  *
  */
class FenetreVoisinageArbitraire : public QDialog

{

    Q_OBJECT
    QLabel* titre;
    QTableWidget* table;
    QPushButton* boutonValider;
    QVBoxLayout* layoutPrincipal;

    int tailleReseau;
    std::vector<std::vector<int>> coordonnees;

public:
    explicit FenetreVoisinageArbitraire(int tailleReseau, QWidget* parent = nullptr);
    std::vector<std::vector<int>> getCoordonnees() {return coordonnees;}
private slots:
    void toggleVoisin(int,int);
};


#endif
