#ifndef REGLE_H
#define REGLE_H
#include"Etat.h"
#include <string>
#include <iostream>
#include<vector>
#include"Voisinage.h"

/**
 * \file          Regle.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definit les differents types de regles.
 *
 */


 /**
  * \class Regle
  * \brief	Classe de base pour les regles, elle doit etre herite dans toutes les autres classes regles.
  *
  *  Pour ajouter une nouvelle Regle, il suffit de creer une nouvelle classe qui herite de Regle, placez dans cette classes les attributs qu'il vous faut afin que la regle puisse se comporter comme vous le desirez.
  *  Il faut ensuite redefinir la methode bool declanchement(), cette fonction renvoit un bool qui vaut true si la regle doit se declancher.
  *
  */
class Regle {
public:
	Regle(Etat Ea, Etat Ef) : Etat_Actuel(Ea), Etat_Final(Ef) {};
	/**
* \fn Regle(Etat Ea, Etat Ef)
* \brief Constructeur de la classe abstraite Regle, constructeur qui initialise l'etat Actuel que doit avoir la cellule pour declancher la regle ainsi que l'etat final que doit prendre la cellule si la regle se declenche.
* \param Ea de type Etat, etat que doit avoir la cellule pour que la regle s'applique.
* \param Ef de type Etat, etat final lorsque la regle s'applique.
*/
    virtual bool declenchement(Etat Ea, std::vector<Etat> V) = 0;
	/**
* \fn virtual bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Methode virtuelle pure, elle servira a savoir si la regle dois se declencher ou non. Elle doit etre redefinie dans les classes filles 
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
	Etat application() { return Etat_Final; };
	/**
* \fn Etat application()
* \brief Methode concrete, elle renvoie l'etat final de la regle. Elle ne doit pas etre redefinie.
* \return Cette fonction retourne un Etat.
*/
protected:
	Etat Etat_Actuel;
	Etat Etat_Final;
};

/**
 * \class Regle_Nb_Voisins
 * \brief	Classe de base pour les regles qui s'appuit sur le nombre de voisins d'une cellule pour qu'elle change d'etat.
 *
 *
 */
class Regle_Nb_Voisins : public Regle {
public:
	Regle_Nb_Voisins(Etat Ea, Etat Ev, Etat Ef, char op, int nb_V) : Regle(Ea,Ef) , Etat_Voisin(Ev), operateur(op), nb_Voisin(nb_V) {};
	/**
* \fn Regle_Nb_Voisins(Etat Ea, Etat Ev, Etat Ef, char op, int nb_V)
* \brief Constructeur de la classe Regle_Nb_Voisins.
* \param Ea de type Etat, etat que doit avoir la cellule pour que la regle s'applique.
* \param Ef de type Etat, etat final lorsque la regle s'applique.
* \param Ev de type Etat, etat des cellules voisine que l'on doit compter dans le voisinage de la cellule.
* \param op de type char, operateur qui servira a comparer le nombre de cellule dans l'etat Ev au nombre de voisins de la regle.
* \param nb_V de type int, nombre de voisin pour lequel la regle s'applique (a mettre en lien avec l'operateur).
*/
	bool declenchement(Etat Ea, std::vector<Etat> V);
	/**
* \fn bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Surcharge de la methode declenchement. Elle retourne true si les conditions de la regle sur le nombre de voisins est verifiee.
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
private:
	Etat Etat_Voisin;
	char operateur;
	int nb_Voisin;
};

/**
 * \class Regle_Auto
 * \brief	Classe de base pour les regles qui s'appuit seulement sur l'etat de la cellule pour changer d'etat.
 *
 *
 */
class Regle_Auto : public Regle {
public:
	Regle_Auto(Etat Ea, Etat Ef) : Regle(Ea,Ef) {};
	/**
* \fn Regle_Auto(Etat Ea, Etat Ef)
* \brief Constructeur de la classe Regle_Auto.
* \param Ea de type Etat, etat que doit avoir la cellule pour que la regle s'applique.
* \param Ef de type Etat, etat final lorsque la regle s'applique.
*/
	bool declenchement(Etat Ea, std::vector<Etat> V);
	/**
* \fn bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Surcharge de la methode declenchement. Elle retourne true si la cellule est dans l'etat Etat_Actuel de la regle.
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
private:
};

/**
 * \class Regle_Motif
 * \brief	Classe de base pour les regles qui s'appuit sur un motif autour de la cellule pour changer d'etat.
 *
 *
 */
class Regle_Motif : public Regle {
public:
	Regle_Motif(Etat Ea, Etat Ef, std::vector<Etat> motif) :Regle(Ea, Ef), motif(motif) {};
	/**
* \fn Regle_Motif(Etat Ea, Etat Ef, std::vector<Etat> motif) :Regle(Ea, Ef)
* \brief Constructeur de la classe Regle_Motif.
* \param Ea de type Etat, etat que doit avoir la cellule pour que la regle s'applique.
* \param Ef de type Etat, etat final lorsque la regle s'applique.
* \param motif de type std::vector<Etat>, vecteur d'etat auquel on comparera le voisinage.
*/
	bool declenchement(Etat Ea, std::vector<Etat> V);
	/**
* \fn bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Surcharge de la methode declenchement. Elle retourne true si le voisinage de la cellule est egal au motif de la regle.
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
protected:
	std::vector<Etat> motif;
};

/**
 * \class Regle_Motif_Langton_Loop
 * \brief	Classe specialement faite pour les regles motif de la Boucle de Langton afin de simplifier son implementation.
 *
 * Mod�le speciale de R�gle motif car l'automate Langton Loop utilise des motif qui prennent en compte les rotations du motif
 * Ce type de r�gle n'est valable que pour l'automate langton loop.
 */

class Regle_Motif_Langton_Loop : public Regle_Motif {
public:
    Regle_Motif_Langton_Loop(Etat Ea, Etat Ef, std::vector<Etat> motif) :Regle_Motif(Ea, Ef, motif) {};
	/**
* \fn Regle_Motif_Langton_Loop(Etat Ea, Etat Ef, std::vector<Etat> motif)
* \brief Constructeur de la classe Regle_Motif_Langton_Loop.
* \param Ea de type Etat, etat que doit avoir la cellule pour que la regle s'applique.
* \param Ef de type Etat, etat final lorsque la regle s'applique.
* \param motif de type std::vector<Etat>, vecteur d'etat auquel on comparera le voisinage.
*/
    bool declenchement(Etat Ea, std::vector<Etat> V);
	/**
* \fn bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Surcharge de la methode declenchement. Cette fonction a ete creer pour l'automate Boucle de Langton, elle ne peut etre utilisee que si l'automate utilise un voisinage de Neumann de rayon 1 ! Elle retourne true si le voisinage de la cellule est egal au motif de la regle (ce type de regle prend en compte les rotations du voisinage).
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
};

/**
 * \class Regle_Motif_Souple
 * \brief	Classe specialement faite pour les regles motif ou les etats dans le motifs peuvent ne pas etre definie.
 *
 * Mod�le speciale de R�gle motif, on peut specifier dans le motif un etat d'indice -1, cela veut dire que cet etat peut etre n'importe quel etat de l'alphabet de l'automate. Ces regles peuvent 
 * etre un gain de temps considerable en fonction de la taille du voisinage.
 * 
 */
class Regle_Motif_Souple : public Regle_Motif
{
public:
    Regle_Motif_Souple(Etat Ea, Etat Ef, std::vector<Etat> motif) :Regle_Motif(Ea, Ef, motif){};
   // Regle telle que les motifs peuvent contenir un etat d'indice -1, n'importe quel etat pourra remplacer le -1 lors du declenchement de la r�gle.
   bool declenchement(Etat Ea, std::vector<Etat> V);
   /**
* \fn bool declenchement(Etat Ea, std::vector<Etat> V)
* \brief Surcharge de la methode declenchement. Elle retourne true si le voisinage de la cellule est egal au motif de la regle, sauf pour les etat du motif d'indice -1 qui peuvent etre 
* n'importe quel etat de l'alphabet de l'automate.
* \param Ea de type Etat, etat de la cellule pour laquelle on regarde si la regle s'applique.
* \param V de type std::vector<Etat>, voisinage de la cellule pour laquelle on regarde si la regle s'applique.
* \return Cette fonction retourne un bool.
*/
};

#endif // !REGLE_H
