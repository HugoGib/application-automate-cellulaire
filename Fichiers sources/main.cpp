#include"Automate.h"
#include"Simulateur.h"
#include <QPushButton>
#include <QApplication>
#include "FenetreAutomate.h"
#include <QCheckBox>
#include <QMenu>
#include <QMenuBar>
#include <QMainWindow>
#include <QDockWidget>
#include <QWidget>


int main(int argc, char* argv[]) {
    QApplication app(argc, argv);

    FenetreAutomate fenetre;
    fenetre.show();

    return app.exec();
}
