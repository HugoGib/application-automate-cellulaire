#ifndef RESEAU_H
#define RESEAU_H
#include"Etat.h"
#include <string>
#include <iostream>
#include<vector>

/**
 * \file          Reseau.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definit ce qu'est un reseau.
 *
 */

 /**
   * \class Reseau
   * \brief	Classe de base pour le reseau.
   *
   *  Le reseau est un objet compose d'etat, on utilise le type std::vector<std::vector<Etat>> pour le representer
   *
   */
class Reseau {
public:
	Reseau(int l, int h) :largeur(l), hauteur(h) {
		Etat etat;
		std::vector<Etat> ligne;
		for (int i = 0; i <= h; i++) {
			ligne = std::vector<Etat>();
			for (int j = 0; j <= l; j++) {
				etat = Etat();
				ligne.push_back(etat);
			}
			reseau.push_back(ligne);
		}
	};
	/**
* \fn Reseau(int l, int h)
* \brief Constructeur de la classe Reseau, constructeur qui initialise le reseau avec des Etat nuls.
* \param l de type int, largeur du reseau que l'on creer.
* \param h de type int, hauteur du reseau que l'on creer.
*/
	Reseau(Etat E, int l, int h) :largeur(l), hauteur(h) {
		std::vector<Etat> ligne;
		for (int i = 0; i <= h; i++) {
			ligne = std::vector<Etat>();
			for (int j = 0; j <= l; j++) {
				ligne.push_back(E);
			}
			reseau.push_back(ligne);
		}
	};
	/**
* \fn Reseau(Etat E, int l, int h)
* \brief Constructeur de la classe Reseau, constructeur qui initialise le reseau a l'etat E.
* \param E de type Etat, Etat avec lequel on initialise le reseau que l'on creer.
* \param l de type int, largeur du reseau que l'on creer.
* \param h de type int, hauteur du reseau que l'on creer.
*/
	/*~Reseau(); Selon moi, pas besoin de destructeur car aucune allocution dynamique*/

	const int getLargeur() const { return largeur; };
	/**
* \fn const int getLargeur() const
* \brief Getter const qui retourne la largeur du reseau.
*
* \return Objet de type int
*/
	const int getHauteur() const { return hauteur; };
	/**
* \fn const int getHauteur() const
* \brief Getter const qui retourne la hauteur du reseau.
*
* \return Objet de type int.
*/
	const Etat getCellule(int x, int y) const { 
		while (x < 0) x += hauteur;
		while (y < 0) y += largeur;
		return(reseau[x%hauteur][y%largeur]); };
	/**
* \fn const Etat getCellule(int x, int y) const
* \brief Getter const qui retourne la cellule du reseau de coordonnees (x,y).
* \param x de type int, ligne de la cellule.
* \param y de type int, colonne de la cellule.
*
* \return Objet de type Etat.
*/
	void setCellule(Etat E, int x, int y) { reseau[x][y] = E; };
	/**
* \fn void setCellule(Etat E, int x, int y)
* \brief Setter qui affecte un etat a la cellule du reseau de coordonnees (x,y).
* \param E de type Etat, Etat que l'on va affecter a la cellule.
* \param x de type int, ligne de la cellule.
* \param y de type int, colonne de la cellule.
*/
	std::vector<std::vector<Etat>> getReseau() { return reseau; };
	/**
* \fn std::vector<std::vector<Etat>> getReseau()
* \brief Getter const qui retourne le reseau.
*
* \return Objet de type std:vector<std::vector<Etat>>.
*/
private:
	std::vector<std::vector<Etat>> reseau;
	int largeur;
	int hauteur;
};

#endif // !RESEAU_H
