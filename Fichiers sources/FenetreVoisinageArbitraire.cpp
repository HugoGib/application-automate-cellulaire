#include "FenetreVoisinageArbitraire.h"
#include <QHeaderView>

FenetreVoisinageArbitraire::FenetreVoisinageArbitraire(int tailleReseau, QWidget* parent):
    tailleReseau(tailleReseau),QDialog(parent)
{
    titre = new QLabel("Selectionner les cellules voisines de la cellule courante jaune :");

    table = new QTableWidget(tailleReseau,tailleReseau,this);
    QHeaderView* header = table ->horizontalHeader();
    header->setMaximumSectionSize(15);
    header = table->verticalHeader();
    header->setMaximumSectionSize(15);

    for (int i = 0; i<tailleReseau ; i++ ) {
       table->setColumnWidth(i,15);
       table->setRowHeight(i,15);
   }

    for(unsigned int i=0; i<tailleReseau; i++)
    {
        for(unsigned int j=0; j<tailleReseau; j++){
            if(i == static_cast<unsigned int>(tailleReseau/2) and j == static_cast<unsigned int>(tailleReseau/2))
            {
                table->setItem(i, j, new QTableWidgetItem("0"));
                table->item(i,j)->setBackground(QColor(255,255,0));
                table->item(i,j)->setForeground(QColor(255,255,0));
            }
            else
            {
                table->setItem(i, j, new QTableWidgetItem("1"));
                table->item(i,j)->setBackground(QColor(0,0,0));
                table->item(i,j)->setForeground(QColor(0,0,0));
            }

        }
    }
    connect(table,SIGNAL(cellClicked(int, int)),this,SLOT(toggleVoisin(int, int)));


    boutonValider = new QPushButton("Valider",this);
    QObject::connect(boutonValider,SIGNAL(clicked()),this,SLOT(accept()));

    layoutPrincipal = new QVBoxLayout(this);
    layoutPrincipal->addWidget(titre);
    layoutPrincipal->addWidget(table);
    layoutPrincipal->addWidget(boutonValider);

    setLayout(layoutPrincipal);
}

void FenetreVoisinageArbitraire::toggleVoisin(int i, int j)
{
    if (table->item(i,j)->text().toInt() == 1)
    {
        table->setItem(i, j, new QTableWidgetItem("2"));
        table->item(i,j)->setBackground(QColor(255,0,0));
        table->item(i,j)->setForeground(QColor(255,0,0));
        std::vector<int> new_coo = {i-static_cast<int>(tailleReseau/2),j-static_cast<int>(tailleReseau/2)};
        coordonnees.push_back(new_coo);
    }
    else if (table->item(i,j)->text().toInt() == 2)
    {
        table->setItem(i, j, new QTableWidgetItem("1"));
        table->item(i,j)->setBackground(QColor(0,0,0));
        table->item(i,j)->setForeground(QColor(0,0,0));
        for(auto k = coordonnees.begin(); k != coordonnees.end();k++)
        {
            if((*k)[0] == i-static_cast<int>(tailleReseau/2) && (*k)[1] == j-static_cast<int>(tailleReseau/2))
            {
                coordonnees.erase(k);
                break;
            }
        }
    }
}
