#ifndef VOISINAGE_H
#define VOISINAGE_H
#include <string>
#include <iostream>
#include<vector>
#include"Etat.h"
#include"Reseau.h"

/**
 * \file          Voisinage.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definition des differents types de voisinage.
 *
 */

 /**
   * \class Voisinage
   * \brief	Classe de base pour le voisinage, elle doit etre herite dans toutes les autres classes voisinage.
   *
   *  Pour ajouter un nouveau Voisinage, il suffit de creer une nouvelle classe qui herite de Voisinage, placez dans cette classes les attributs qu'il vous faut afin que le voisinage puisse se comporter comme vous le desirez.
   *  Il faut ensuite redefinir la methode std::vector<Etat> voisins(), cette fonction renvoit le voisinage d'une cellule dans un reseau de type std::vector<Etat>.
   *  Il faut aussi redefinir la methode std::vector<std::vector<int>> cordonneesVoisins(), cette fonction doit renvoyer les coordonnees des cellules voisines par rapport a une cellule quelconque du reseau,
   *  mettons que la cellule dont on cherche les voisins a pour coordonnees (x,y) et qu'une cellule voisine a (x,y) ait pour coordonnees (i,j) alors le vector retournee par cordonneesVoisins() doit contenir (i-x , j-y).
   *
   */
class Voisinage {
public:
	virtual std::vector<Etat> voisins(const Reseau& R, int i, int j) = 0;
	/**
* \fn virtual std::vector<Etat> voisins(const Reseau& R, int i, int j)
* \brief Methode virtuelle pure, elle servira a calculer le voisinage d'une cellule. Elle doit etre redefinie dans les classes filles.
* \param R de type Reseau, reseau dans lequel on calcule le voisinage.
* \param i de type int, ligne de la cellule dont on calcule le voisinage.
* \param j de type int, colonne de la cellule dont on calcule le voisinage.
* \return Cette fonction retourne un std::vector<Etat>.
*/
    virtual std::vector<std::vector<int>> cordonneesVoisins() = 0;
    /**
* \fn std::vector<std::vector<int>> cordonneesVoisins()
* \brief Methode virtuelle pure, cette fonction calcule les coordonnees relatives des voisins par rapport aux coordonnees de n'importe quelle cellule. Elle doit etre redefinie dans les classes filles.
* \return Cette fonction retourne un std::vector<int>.
*/
};

/**
  * \class Voisinage_Moore
  * \brief	Classe du voisinage de Moore.
  *
  */
class Voisinage_Moore : public Voisinage {
private:
	int rayon;
public:
	Voisinage_Moore(int r) : rayon(r) {};
	/**
* \fn Voisinage_Moore(int r)
* \brief Constructeur de la classe Voisinage_Moore.
* \param r de type int, rayon du voisinage.
*/
	std::vector<Etat> voisins(const Reseau& R, int i, int j);
	/**
* \fn std::vector<Etat> voisins(const Reseau& R, int i, int j)
* \brief Surcharge de la fonction voisins. Elle calcule le voisinage de Moore de rayon r d'une cellule.
* \param R de type Reseau, reseau dans lequel on calcule le voisinage.
* \param i de type int, ligne de la cellule dont on calcule le voisinage.
* \param j de type int, colonne de la cellule dont on calcule le voisinage.
* \return Cette fonction retourne un std::vector<Etat>.
*/
    std::vector<std::vector<int>> cordonneesVoisins();
    /**
* \fn std::vector<std::vector<int>> cordonneesVoisins()
* \brief Surcharge de cordonneesVoisins, cette fonction calcule les coordonnees relatives des voisins par rapport aux coordonnees de n'importe quelle cellule.
* \return Cette fonction retourne un std::vector<int>.
*/
};

/**
  * \class Voisinage_Neumann
  * \brief	Classe du voisinage de Von-Neumann.
  *
  */
class Voisinage_Neumann : public Voisinage {
private:
	int rayon;
public:
	Voisinage_Neumann(int r) :rayon(r) {};
	/**
* \fn Voisinage_Neumann(int r)
* \brief Constructeur de la classe Voisinage_Neumann.
* \param r de type int, rayon du voisinage.
*/
	std::vector<Etat> voisins(const Reseau& R, int i, int j);
	/**
* \fn std::vector<Etat> voisins(const Reseau& R, int i, int j)
* \brief Surcharge de la fonction voisins. Elle calcule le voisinage de Neumann de rayon r d'une cellule.
* \param R de type Reseau, reseau dans lequel on calcule le voisinage.
* \param i de type int, ligne de la cellule dont on calcule le voisinage.
* \param j de type int, colonne de la cellule dont on calcule le voisinage.
* \return Cette fonction retourne un std::vector<Etat>.
*/
    std::vector<std::vector<int>> cordonneesVoisins();
    /**
* \fn std::vector<std::vector<int>> cordonneesVoisins()
* \brief Surcharge de cordonneesVoisins, cette fonction calcule les coordonnees relatives des voisins par rapport aux coordonnees de n'importe quelle cellule.
* \return Cette fonction retourne un std::vector<int>.
*/
};

/**
  * \class Voisinage_Arbitraire
  * \brief	Classe du voisinage arbitraire.
  *
  */
class Voisinage_Arbitraire : public Voisinage{
private:
	std::vector<std::vector<int>> coordonnees;
public:
	Voisinage_Arbitraire(std::vector<std::vector<int>> Coordonnees) : coordonnees(Coordonnees) {};
	/**
* \fn Voisinage_Arbitraire(std::vector<std::vector<int>> Coordonnees)
* \brief Constructeur de la classe Voisinage_Arbitraire.
* \param Coordonnees de type std::vector<std::vector<int>>, vector de couple de d'int qui informent sur la position relative des voisins par rapport a la cellule
*/
	std::vector<Etat> voisins(const Reseau& R, int i, int j);
	/**
* \fn std::vector<Etat> voisins(const Reseau& R, int i, int j)
* \brief Surcharge de la fonction voisins. Elle calcule le voisinage arbitraire d'une cellule.
* \param R de type Reseau, reseau dans lequel on calcule le voisinage.
* \param i de type int, ligne de la cellule dont on calcule le voisinage.
* \param j de type int, colonne de la cellule dont on calcule le voisinage.
* \return Cette fonction retourne un std::vector<Etat>.
*/
    std::vector<std::vector<int>> cordonneesVoisins();
    /**
* \fn std::vector<std::vector<int>> cordonneesVoisins()
* \brief Surcharge de cordonneesVoisins, cette fonction calcule les coordonnees relatives des voisins par rapport aux coordonnees de n'importe quelle cellule.
* \return Cette fonction retourne un std::vector<int>.
*/
};

/**
  * \class Voisinage_Langton_Loop
  * \brief	Classe du voisinage specifique pour faciliter l'implementation de la Boucle de Langton.
  *
  */
class Voisinage_Langton_Loop : public Voisinage {
private:
public:
    std::vector<Etat> voisins(const Reseau& R, int i, int j);
    /**
* \fn std::vector<Etat> voisins(const Reseau& R, int i, int j)
* \brief Surcharge de la fonction voisins. Elle calcule le voisinage de Neumann de rayon r d'une cellule.
* \param R de type Reseau, reseau dans lequel on calcule le voisinage.
* \param i de type int, ligne de la cellule dont on calcule le voisinage.
* \param j de type int, colonne de la cellule dont on calcule le voisinage.
* \return Cette fonction retourne un std::vector<Etat>.
*/
    std::vector<std::vector<int>> cordonneesVoisins();
    /**
* \fn std::vector<std::vector<int>> cordonneesVoisins()
* \brief Surcharge de cordonneesVoisins, cette fonction calcule les coordonnees relatives des voisins par rapport aux coordonnees de n'importe quelle cellule.
* \return Cette fonction retourne un std::vector<int>.
*/
};

#endif //!VOISINAGE_H
