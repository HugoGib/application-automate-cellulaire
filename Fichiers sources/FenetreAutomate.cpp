#include"FenetreAutomate.h"
#include"FenetreReseau.h"
#include"Automate.h"

FenetreAutomate::FenetreAutomate(QWidget* parent):QWidget(parent)
{

    automateComboBox = new QComboBox;
    automateComboBox->addItem("Life Game");
    automateComboBox->addItem("Brian Brain");
    automateComboBox->addItem("Griffeath");
    automateComboBox->addItem("Langton Loop");
    automateComboBox->addItem("Fourmi de Langton");
    automateComboBox->addItem("Automate Utilisateur");

    automateLabel = new QLabel("Selectionnez un automate");

    automateLayout=new QVBoxLayout;
    automateLayout->addWidget(automateLabel);
    automateLayout->addWidget(automateComboBox);

    spinboxLabel = new QLabel("Taille de la grille");

    spinboxLayout = new QVBoxLayout;

    spinBoxTailleReseau = new QSpinBox;
    spinBoxTailleReseau->setRange(30, 200);
    spinBoxTailleReseau->setSingleStep(1);
    spinBoxTailleReseau->setValue(30);

    spinboxLayout->addWidget(spinboxLabel);
    spinboxLayout->addWidget(spinBoxTailleReseau);


    LayoutPrincipal = new QHBoxLayout;
    LayoutIntermediaire = new QVBoxLayout;
    LayoutIntermediaire->addLayout(automateLayout);
    LayoutIntermediaire->addLayout(spinboxLayout);

    boutonAutomate = new QPushButton("Valider",this);
    LayoutPrincipal->addLayout(LayoutIntermediaire);
    LayoutPrincipal->addWidget(boutonAutomate);


    setLayout(LayoutPrincipal);
    QObject::connect(boutonAutomate, SIGNAL(clicked()), this, SLOT(createAutomate()));
}

void FenetreAutomate::createAutomate(){
    Automate* automate;
    int i = automateComboBox->currentIndex();
    unsigned int tailleReseau = spinBoxTailleReseau->value();
    if(i == 0){
        automate = new Automate_Life_Game();
    }
    else if(i == 1){
        automate = new Automate_Brian_Brain();
        }
    else if(i == 2){
        automate = new Automate_Griffeath();
    }
    else if(i == 3){
        automate = new Automate_Langton_Loop();
    }
    else if(i == 4){
        automate = new Automate_Fourmi_Langton();
    }
    else if(i == 5){ //Il s'agit de l'automate utilisateur, c'est un cas particulier de la factory
        FenetreAlphabet* fenetre_alphabet = new FenetreAlphabet(tailleReseau);
        fenetre_alphabet->show();
        hide();
        return;
    }
    else{
        exit(EXIT_FAILURE);
    }
    FenetreReseau* fenetreRes = new FenetreReseau(automate,tailleReseau);
    fenetreRes->show();
    hide();
}

