#include "FenetreReseau.h"


FenetreReseau::FenetreReseau(Automate* A, unsigned int tailleReseau, QWidget* parent):QWidget(parent), automate(A), tailleReseau(tailleReseau)
{
    int taille = 25;
    int tailleRes = tailleReseau;
    reseau = new Reseau(automate->getAlphabet()[0] ,tailleRes,tailleRes);

    res = new QTableWidget(tailleRes,tailleRes);

    QHeaderView* header = res ->horizontalHeader();
    header->setMaximumSectionSize(15);

    header = res ->verticalHeader();
    header->setMaximumSectionSize(15);

    LabelLayout = new QVBoxLayout;
    LayoutH = new QHBoxLayout;

    ResLayout = new QVBoxLayout;
    ResLabel = new QLabel("Configuration du Reseau Initial");

     for (int i = 0; i<tailleRes ; i++ ) {
        res->setColumnWidth(i,taille);
        res->setRowHeight(i,taille);
    }
    for(int i = 0; i < automate->getAlphabet().size();i++){
        const QString str = QString::fromStdString(std::to_string(i)+" : "+automate->getAlphabet()[i].getLabel());
        QLabel* label = new QLabel(str);
        LabelLayout->addWidget(label);
    }


    reset();

    res->horizontalHeader()->setVisible(false); //Pas de nom pour les colonnes.
    res->verticalHeader()->setVisible(false);
    connect(res,SIGNAL(cellChanged(int, int)),this,SLOT(cellActivation(int, int)));

    boutonInitialisation = new QPushButton("Initaliser",this);
    QObject::connect(boutonInitialisation, SIGNAL(clicked()), this, SLOT(initialisation()));
    boutonReset = new QPushButton("Reset",this);
    QObject::connect(boutonReset, SIGNAL(clicked()), this, SLOT(reset()));
    boutonReseau = new QPushButton("Valider",this);
    QObject::connect(boutonReseau, SIGNAL(clicked()), this, SLOT(createSimulation()));

    LabelLayout->addWidget(boutonInitialisation);
    LabelLayout->addWidget(boutonReset);
    LabelLayout->addWidget(boutonReseau);

    LayoutH->addWidget(res);
    LayoutH->addLayout(LabelLayout);

    ResLayout->addWidget(ResLabel);
    ResLayout->addLayout(LayoutH);
    setLayout(ResLayout);
}

void FenetreReseau::cellActivation(int row, int column)
{
    int indice = res->item(row,column)->text().toInt();
    std::vector<Etat> alphabet = automate->getAlphabet();
    if(indice>=0 && indice < alphabet.size() ){
        int r = alphabet[indice].getCouleur().getR();
        int g = alphabet[indice].getCouleur().getG();
        int b = alphabet[indice].getCouleur().getB();
        res->item(row, column)->setBackground(QColor(r, g, b));
        res->item(row, column)->setForeground(QColor(r, g, b));
        reseau->setCellule(alphabet[indice],row,column);
    }
    else{
        res->setItem(row, column, new QTableWidgetItem("0"));
        int r = alphabet[0].getCouleur().getR();
        int g = alphabet[0].getCouleur().getG();
        int b = alphabet[0].getCouleur().getB();
        res->item(row, column)->setBackground(QColor(r, g, b));
        res->item(row, column)->setForeground(QColor(r, g, b));
        reseau->setCellule(alphabet[0],row,column);
    }
}


void FenetreReseau::createSimulation(){
    Simulateur* simulateur = Simulateur::getInstance(automate, reseau);
    FenetreSimulation* fenetreSimu = new FenetreSimulation(simulateur);
    fenetreSimu->show();
    hide();
}

void FenetreReseau::reset(){
    std::vector<Etat> alphabet = automate->getAlphabet();
    for(unsigned int i=0; i<tailleReseau; i++)
    {
        for(unsigned int j=0; j<tailleReseau; j++){
            res->setItem(i, j, new QTableWidgetItem("0"));
            int r0 = automate->getAlphabet()[0].getCouleur().getR();
            int g0 = automate->getAlphabet()[0].getCouleur().getG();
            int b0 = automate->getAlphabet()[0].getCouleur().getB();
            res->item(i,j)->setBackground(QColor(r0, g0, b0)); //Si ne marche pas : setBackground(Qt::white)
            res->item(i,j)->setForeground(QColor(r0, g0, b0)); //Si ne marche pas : setForeground(Qt::white)
            reseau->setCellule(alphabet[0],i,j);
        }
    }
}

void FenetreReseau::initialisation(){
    Reseau nouveau_reseau = automate->initialisation(tailleReseau,tailleReseau);
    std::vector<Etat> alphabet = automate->getAlphabet();
    unsigned int indice;
    for(unsigned int i=0; i<tailleReseau; i++)
    {
        for(unsigned int j=0; j<tailleReseau; j++){
            indice = nouveau_reseau.getCellule(i,j).getIndice();
            res->setItem(i, j, new QTableWidgetItem(QString::number(indice)));
            int r0 = automate->getAlphabet()[indice].getCouleur().getR();
            int g0 = automate->getAlphabet()[indice].getCouleur().getG();
            int b0 = automate->getAlphabet()[indice].getCouleur().getB();
            res->item(i,j)->setBackground(QColor(r0, g0, b0)); //Si ne marche pas : setBackground(Qt::white)
            res->item(i,j)->setForeground(QColor(r0, g0, b0)); //Si ne marche pas : setForeground(Qt::white)
            reseau->setCellule(alphabet[indice],i,j);
        }
    }
    ;
}
