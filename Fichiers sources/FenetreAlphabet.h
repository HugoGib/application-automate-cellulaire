/**
 * \file          FenetreRegleMotif.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un alphabet pour l'automate utilisateur.
 *
 */
#ifndef FENETRE_ALPHABET
#define FENETRE_ALPHABET

#include "Etat.h"
#include <vector>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QPushButton>


 /**
  * \class FenetreAlphabet
  * \brief	Classe pour creer une fenetre pour creer un alphabet pour l'automate utilisateur.
 *
  *
  *
  */
class FenetreAlphabet : public QWidget
{
    Q_OBJECT
    QVBoxLayout* layoutGeneral;
    QHBoxLayout* layoutAjoutEtat;
    QListWidget* listeEtats;
    QPushButton* boutonValider;
    QPushButton* boutonAjouter;
    QPushButton* boutonSupprimer;
    unsigned int tailleReseau;
    std::vector<Etat> alphabet;

public:
    explicit FenetreAlphabet(unsigned int tailleReseau, QWidget* parent = nullptr);

private slots:
    void valider();
    void ajouterEtat();
    void supprimerEtat();
};


#endif
