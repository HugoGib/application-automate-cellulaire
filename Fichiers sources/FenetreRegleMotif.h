/**
 * \file          FenetreRegleMotif.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un motif qui sera utilise dans une regle motif.
 *
 */
#ifndef FENETREREGLEMOTIF_H
#define FENETREREGLEMOTIF_H
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QModelIndex>
#include <QTableWidget>
#include <QHeaderView>
#include "Voisinage.h"
#include "Etat.h"
#include <QDialog>


 /**
  * \class FenetreRegleMotif
  * \brief	Classe pour creer une fenetre pour creer un motif qui sera utilise dans une regle motif.
  *
  *
  */
class FenetreRegleMotif : public QDialog
{
    Q_OBJECT
    QHBoxLayout* Layout;
    QVBoxLayout* BoutonLayout;
    QLabel* MatriceLabel;
    QHBoxLayout* LayoutH;
    QPushButton* boutonValidation;
    QPushButton* boutonAnnulation;
    QTableWidget* matrice;
    QPushButton* boutonReset;
    int tailleReseau;
    Voisinage* voisinage;
    std::vector<Etat> alphabet;
    std::vector<Etat> motif;
    unsigned int x;
    unsigned int y;

public:
    explicit FenetreRegleMotif(std::vector<Etat> alphabet, unsigned int tailleReseau ,Voisinage* voisinage, QWidget* parent = nullptr);
    std::vector<Etat> getMotif();

private slots:
    void cellActivation(int row, int column);
    void reset();
};

#endif // FENETREREGLEMOTIF_H
