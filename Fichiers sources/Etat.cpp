#include"Etat.h"

/*COULEUR*/

void Couleur::setCouleur(int r, int g, int b) {
	R = r;
	G = g;
	B = b;
}


/*ETAT*/

void Etat::setEtat(int r, int g, int b, int i, std::string l) {
	couleur.setCouleur(r, g, b);
	indice = i;
	label = l;
}

void Etat::setEtat(int r, int g, int b, int i) {
	couleur.setCouleur(r, g, b);
	indice = i;
	label = "0";
}

bool Etat::operator==(Etat E) const {
	return  this->indice == E.getIndice();
}

bool Etat::operator!=(Etat E) const {
	return  this->indice != E.getIndice();
}