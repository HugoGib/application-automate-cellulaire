#ifndef ETAT_H
#define ETAT_H
#include <string>
#include <iostream>
#include<vector>

/**
 * \file          Etat.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definit ce qu'est un etat et une couleur.
 *
 */

 /**
  * \class Couleur
  * \brief	Classe qui va contenir une intensite en Rouge, Vert et Bleu
  *
  *
  */
class Couleur {
public:
	Couleur() : R(0), G(0), B(0) {};
	/**
* \fn Couleur()
* \brief Constructeur d'un objet couleur, utilisees par les etats
*/
    int getR() const { return R; };
    int getG() const { return G; };
    int getB() const { return B; };
	void setCouleur(int r, int g, int b);
	/**
 * \fn setCouleur(int r, int g, int b)
 * \brief Fonction qui modifie une couleur.
 * \param r de type int, intensite de rouge pour la couleur.
*  \param g de type int, intensite de vert pour la couleur.
*  \param b de type int, intensite de bleu pour la couleur.
 */
private:
	int R;
	int G;
	int B;
};


/**
 * \class Etat
 * \brief	Classe qui definie l'etat d'une cellule
 *
 *  Une cellule est composee d'une couleur, d'un indice et d'un label
 *
 */
class Etat {
public:
	void setEtat(int r, int g, int b, int i, std::string l);
	void setEtat(int r, int g, int b, int i);
	Etat(int r, int g, int b, int i) : indice(i), label("0") {
		couleur.setCouleur(r, g, b);
	}
	/**
* \fn Etat(int r, int g, int b, int i)
* \brief Constructeur de la classe Etat, constructeur qui initialise un etat avec une couleur et un indice, le label de l'etat est nul.
* \param r de type int, intensite de rouge pour la couleur de l'etat.
* \param g de type int, intensite de vert pour la couleur de l'etat.
* \param b de type int, intensite de bleu pour la couleur de l'etat.
* \param i de type int, indice de l'etat.
*/
	Etat() : couleur(Couleur()), indice(0), label("") {};
	/**
* \fn Etat()
* \brief Constructeur de la classe Etat, constructeur qui initialise un etat avec une couleur et un indice et un label, tous nuls.
*/
	Etat(int r, int g, int b, int i, std::string l) : indice(i), label(l) {
		couleur.setCouleur(r, g, b);
	}
	/**
* \fn Etat(int r, int g, int b, int i, std::string l)
* \brief Constructeur de la classe Etat, constructeur qui initialise un etat avec une couleur et un indice et un label.
* \param r de type int, intensite de rouge pour la couleur de l'etat.
* \param g de type int, intensite de vert pour la couleur de l'etat.
* \param b de type int, intensite de bleu pour la couleur de l'etat.
* \param i de type int, indice de l'etat.
* \param i de type std::string, label de l'etat.
*/
	bool operator==(Etat E) const;
	/**
* \fn operator==(Etat E) const
* \brief Surcharge de l'operateur == afin de pouvoir comparer deux etats entre eux, cette fonction retourne true si les deux etats on le m�me indice.
* \param E de type Etat, etat avec lequel on compare l'etat qui appelle cette fonction.
* \return Cette fonction retourne un bool.
*/
	bool operator!=(Etat E) const;
	/**
* \fn operator==(Etat E) const
* \brief Surcharge de l'operateur =! afin de pouvoir comparer deux etats entre eux, cette fonction retourne false si les deux etats on le m�me indice.
* \param E de type Etat, etat avec lequel on compare l'etat qui appelle cette fonction.
* \return Cette fonction retourne un bool.
*/
	const Couleur getCouleur() const { return(couleur); };
	/**
 * \fn std::vector<Etat> getCouleur() const
 * \brief Getter const de la couleur de l'etat.
 *
 * \return objet de type Couleur.
 */
	const int getIndice() const { return indice; };
	/**
 * \fn std::vector<Etat> getIndice() const
 * \brief Getter const de l'indice de l'etat.
 *
 * \return objet de type int.
 */
	const std::string getLabel() const { return label; };
	/**
 * \fn std::vector<Etat> getLabel() const
 * \brief Getter const du label de l'etat.
 *
 * \return objet de type std::string.
 */
private:
	Couleur couleur;
	int indice;
	std::string label;
};

#endif // !ETAT_H
