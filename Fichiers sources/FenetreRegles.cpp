#include "FenetreRegles.h"
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>
#include "Automate.h"
#include "FenetreReseau.h"
#include "FenetreRegleMotif.h"

FenetreRegles::FenetreRegles(unsigned int tailleReseau, std::vector<Etat> alphabet, Voisinage* voisinage, QWidget* parent):tailleReseau(tailleReseau),alphabet(alphabet),voisinage(voisinage),QWidget(parent),regles(0)
{

    listeRegles = new QListWidget(this);

    boutonValider = new QPushButton("Valider",this);
    QObject::connect(boutonValider, SIGNAL(clicked()), this, SLOT(valider()));
    boutonAjouterRegleNbVoisins = new QPushButton("Ajouter règle nombre de voisins",this);
    QObject::connect(boutonAjouterRegleNbVoisins, SIGNAL(clicked()), this, SLOT(ajouterRegleNbVoisins()));
    boutonAjouterRegleAuto  = new QPushButton("Ajouter règle automatique",this);
    QObject::connect(boutonAjouterRegleAuto, SIGNAL(clicked()), this, SLOT(ajouterRegleAuto()));
    boutonAjouterRegleMotif = new QPushButton("Ajouter règle motif",this);
    QObject::connect(boutonAjouterRegleMotif, SIGNAL(clicked()), this, SLOT(ajouterRegleMotif()));
    boutonMonterPriorite = new QPushButton("Monter priorité",this);
    QObject::connect(boutonMonterPriorite, SIGNAL(clicked()), this, SLOT(monterPriorite()));
    boutonBaisserPriorite = new QPushButton("Baisser priorité",this);
    QObject::connect(boutonBaisserPriorite, SIGNAL(clicked()), this, SLOT(baisserPriorite()));
    boutonSupprimer  = new QPushButton("Supprimer règle",this);
    QObject::connect(boutonSupprimer, SIGNAL(clicked()), this, SLOT(supprimerRegle()));

    layoutAjoutRegles = new QVBoxLayout();
    layoutAjoutRegles->addWidget(boutonAjouterRegleAuto);
    layoutAjoutRegles->addWidget(boutonAjouterRegleNbVoisins);
    layoutAjoutRegles->addWidget(boutonAjouterRegleMotif);
    layoutAjoutRegles->addWidget(boutonMonterPriorite);
    layoutAjoutRegles->addWidget(boutonBaisserPriorite);
    layoutAjoutRegles->addWidget(boutonSupprimer);

    layoutGeneral = new QHBoxLayout();
    layoutGeneral->addWidget(listeRegles);
    layoutGeneral->addLayout(layoutAjoutRegles);

    layoutMain = new QVBoxLayout();
    layoutMain->addLayout(layoutGeneral);
    layoutMain->addWidget(boutonValider);

    setLayout(layoutMain);
}

void FenetreRegles::valider(){
    Automate_Utilisateur* automate = new Automate_Utilisateur(alphabet,regles,voisinage);
    FenetreReseau* fenetreRes = new FenetreReseau(automate,tailleReseau);
    fenetreRes->show();
    hide();
}

void FenetreRegles::ajouterRegleNbVoisins()
{
    bool ok;
    QStringList etats;
    for(int i = 0; i<alphabet.size(); i++)
    {
        etats << QString::number(alphabet[i].getIndice())+" : "+QString::fromStdString(alphabet[i].getLabel());
    }

    QString txtEtatInitial = QInputDialog::getItem(this, "Création d'une règle nombre de voisins",
                                         tr("Etat initial de la cellule avant application de la règle:"), etats,
                                        0,false, &ok);
    if (!ok)
    {
        QMessageBox msgBox;
        msgBox.setText("L'etat doit être choisi.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
    }
    else
    {
        int EtatInitial = txtEtatInitial.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
        // ici on a l'état initial
        QString txtEtatFinal = QInputDialog::getItem(this, "Création d'une règle nombre de voisins",
                                             tr("Etat final de la cellule après application de la règle :"), etats,
                                            0,false, &ok);
        if (!ok)
        {
            QMessageBox msgBox;
            msgBox.setText("L'etat doit être choisi.");
            msgBox.setStandardButtons(QMessageBox::Cancel);
            msgBox.exec();
        }
        else
        {
            int EtatFinal = txtEtatFinal.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
            QString txtEtatVoisin = QInputDialog::getItem(this, "Création d'une règle nombre de voisins",
                                                 tr("Etat des cellules voisines à compter :"), etats,
                                                0,false, &ok);
            if (!ok)
            {
                QMessageBox msgBox;
                msgBox.setText("L'etat doit être choisi.");
                msgBox.setStandardButtons(QMessageBox::Cancel);
                msgBox.exec();
            }
            else
            {
                int EtatVoisin = txtEtatVoisin.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
                QStringList operateurs;
                operateurs << tr("<") << tr("=") << tr(">");
                QString txtOperateur = QInputDialog::getItem(this, "Création d'une règle nombre de voisins",
                                                     tr("Choisir l'opérateur de comparaison (nombre de voisins ? k)"), operateurs,
                                                    0,false, &ok);
                if (!ok)
                {
                    QMessageBox msgBox;
                    msgBox.setText("L'opérateur de comparaison n'est pas valide.");
                    msgBox.setStandardButtons(QMessageBox::Cancel);
                    msgBox.exec();
                }
                else{
                    char operateur = txtOperateur.toStdString()[0];

                    int nbVoisins = QInputDialog::getInt(this, "Création d'une règle nombre de voisins",
                                                         tr("Choisir le nombre de voisins avec lequel il faut comparer : "), 0,
                                                        0,voisinage->cordonneesVoisins().size(),1, &ok);
                    if (!ok)
                    {
                        QMessageBox msgBox;
                        msgBox.setText("Le nombre de voisins n'est pas valide.");
                        msgBox.setStandardButtons(QMessageBox::Cancel);
                        msgBox.exec();
                    }
                    else{
                        std::cout << alphabet[EtatInitial].getIndice() << "," << alphabet[EtatVoisin].getIndice() << "," << alphabet[EtatFinal].getIndice() << ","<<operateur <<","<<nbVoisins<<"\n";
                        regles.push_back(new Regle_Nb_Voisins(alphabet[EtatInitial],alphabet[EtatVoisin],alphabet[EtatFinal],operateur,nbVoisins));
                        listeRegles->addItem(tr("Regle NbVoisins ")+txtEtatInitial+tr(" -> ")+txtEtatFinal+tr(" si  nb(")+txtEtatVoisin+tr(") ")+txtOperateur+QString::number(nbVoisins));
                    }
                }
            }
        }
    }
}

void FenetreRegles::ajouterRegleAuto()
{
    bool ok;
    QStringList etats;
    for(int i = 0; i<alphabet.size(); i++)
    {
        etats << QString::number(alphabet[i].getIndice())+" : "+QString::fromStdString(alphabet[i].getLabel());
    }

    QString txtEtatInitial = QInputDialog::getItem(this, "Création d'une règle automatique",
                                         tr("Etat initial de la cellule avant application de la règle:"), etats,
                                        0,false, &ok);
    if (!ok)
    {
        QMessageBox msgBox;
        msgBox.setText("L'etat doit être choisi.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
    }
    else
    {
        int EtatInitial = txtEtatInitial.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
        // ici on a l'état initial
        QString txtEtatFinal = QInputDialog::getItem(this, "Création d'une règle automatique",
                                             tr("Etat final de la cellule après application de la règle :"), etats,
                                            0,false, &ok);
        if (!ok)
        {
            QMessageBox msgBox;
            msgBox.setText("L'etat doit être choisi.");
            msgBox.setStandardButtons(QMessageBox::Cancel);
            msgBox.exec();
        }
        else
        {
            int EtatFinal = txtEtatFinal.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
            regles.push_back(new Regle_Auto(alphabet[EtatInitial],alphabet[EtatFinal]));
            listeRegles->addItem(tr("Regle Auto ")+txtEtatInitial+tr(" -> ")+txtEtatFinal);
        }
     }
}

void FenetreRegles::ajouterRegleMotif()
{
    auto fen = new FenetreRegleMotif(alphabet,tailleReseau,voisinage,this);
    fen->exec();
    if(fen->Accepted)
    {
        auto motif = fen->getMotif();
        bool ok;
        QStringList etats;
        for(int i = 0; i<alphabet.size(); i++)
        {
            etats << QString::number(alphabet[i].getIndice())+" : "+QString::fromStdString(alphabet[i].getLabel());
        }

        QString txtEtatInitial = QInputDialog::getItem(this, "Création d'une règle automatique",
                                             tr("Etat initial de la cellule avant application de la règle:"), etats,
                                            0,false, &ok);
        if (!ok)
        {
            QMessageBox msgBox;
            msgBox.setText("L'etat doit être choisi.");
            msgBox.setStandardButtons(QMessageBox::Cancel);
            msgBox.exec();
        }
        else
        {
            int EtatInitial = txtEtatInitial.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
            // ici on a l'état initial
            QString txtEtatFinal = QInputDialog::getItem(this, "Création d'une règle automatique",
                                                 tr("Etat final de la cellule après application de la règle :"), etats,
                                                0,false, &ok);
            if (!ok)
            {
                QMessageBox msgBox;
                msgBox.setText("L'etat doit être choisi.");
                msgBox.setStandardButtons(QMessageBox::Cancel);
                msgBox.exec();
            }
            else
            {
                int EtatFinal = txtEtatFinal.split(" : ")[0].toInt(); // on récupère l'indice de l'état choisi
                regles.push_back(new Regle_Motif(alphabet[EtatInitial],alphabet[EtatFinal],motif));
                listeRegles->addItem(tr("Regle Motif ")+txtEtatInitial+tr(" -> ")+txtEtatFinal);
            }
         }
    }
}

void FenetreRegles::monterPriorite(){
    auto currentItem = listeRegles->currentItem();
    int row = listeRegles->row(currentItem);
    if(row > 0)
    {
        Regle* backup = regles[row];
        regles[row] = regles[row-1];
        regles[row-1] = backup;
        auto item = listeRegles->takeItem(row);
        listeRegles->insertItem(row-1,item);
        listeRegles->setCurrentItem(item);
    }

}
void FenetreRegles::baisserPriorite(){
    auto currentItem = listeRegles->currentItem();
    int row = listeRegles->row(currentItem);
    if(row < listeRegles->count()-1)
    {
        Regle* backup = regles[row];
        regles[row] = regles[row+1];
        regles[row+1] = backup;
        auto item = listeRegles->takeItem(row);
        listeRegles->insertItem(row+1,item);
        listeRegles->setCurrentItem(item);
    }
}
void FenetreRegles::supprimerRegle(){
    auto currentItem = listeRegles->currentItem();
    if(currentItem != nullptr)
    {
        int row = listeRegles->row(currentItem);
        listeRegles->takeItem(row);
        std::vector<Regle*>::iterator it;
        for(it = regles.begin(); row>0;it++)row--;
        regles.erase(it);
    }
}
