/**
 * \file          FenetreAutomate.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un automate pour creer une simulation.
 *
 */
#ifndef FENETREAUTOMATE_H
#define FENETREAUTOMATE_H
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QGroupBox>
#include "FenetreAlphabet.h"


 /**
  * \class FenetreAutomate
  * \brief	Classe pour creer une fenetre pour creer un automate pour creer une simulation.
 *
  *
  *
  */
class FenetreAutomate : public QWidget

{

    Q_OBJECT
    QComboBox* automateComboBox;
    QVBoxLayout* automateLayout;
    QHBoxLayout* LayoutPrincipal;
    QVBoxLayout* LayoutIntermediaire;
    QLabel* automateLabel;
    QPushButton* boutonAutomate;
    QVBoxLayout* spinboxLayout;
    QLabel* spinboxLabel;
    QSpinBox* spinBoxTailleReseau;

public:
    explicit FenetreAutomate(QWidget* parent = nullptr);
private slots:
    void createAutomate();
};

#endif // FENETREAUTOMATE_H
