/**
 * \file          FenetreVoisinage.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un type de voisinage pour l'automate utilisateur.
 *
 */
#ifndef FENETRE_VOISINAGE
#define FENETRE_VOISINAGE

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <vector>
#include <QVBoxLayout>
#include "Etat.h"
#include "Voisinage.h"
#include "FenetreVoisinageArbitraire.h"

 /**
  * \class FenetreVoisinage
  * \brief	Classe pour creer une fenetre pour creer un type de voisinage pour l'automate utilisateur.
  *
  *
  */
class FenetreVoisinage : public QWidget

{

    Q_OBJECT
    QLabel* typeVoisinage;
    QComboBox* typeVoisinageComboBox;
    QPushButton* boutonValider;
    QVBoxLayout* layoutPrincipal;

    int tailleReseau;
    std::vector<Etat> alphabet;
    Voisinage* voisinage;

public:
    explicit FenetreVoisinage(int tailleReseau, std::vector<Etat> alphabet, QWidget* parent = nullptr);
private :
    Voisinage* creerVoisinageNeuman();
    Voisinage* creerVoisinageMoore();
    Voisinage* creerVoisinageArbitraire();
private slots:
    void creerVoisinage();
};


#endif
