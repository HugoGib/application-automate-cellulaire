#include "Simulateur.h"
Simulateur* Simulateur::instance = nullptr;

Simulateur* Simulateur::getInstance(Automate* automate, Reseau* reseau){
    if (Simulateur::instance == nullptr){
        Simulateur::instance = new Simulateur(automate, reseau);
    }
    return (Simulateur::instance);
}

void Simulateur::reset()
{
    Reseau* nouveauReseau = new Reseau(reseau->getLargeur(),reseau->getHauteur());
    for(int i=0;i<reseau->getLargeur();i++)
    {
        for(int j = 0; j < reseau->getHauteur();j++)
        {
            nouveauReseau->setCellule(reseau_initial.getCellule(i,j),i,j);
        }
    }
    delete reseau;
    reseau = nouveauReseau;
}
