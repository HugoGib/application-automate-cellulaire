#include "FenetreRegleMotif.h"
#include <QMessageBox>

FenetreRegleMotif::FenetreRegleMotif(std::vector<Etat> alphabet, unsigned int tailleReseau,Voisinage* voisinage, QWidget* parent) : alphabet(alphabet), tailleReseau(tailleReseau), voisinage(voisinage),QDialog(parent)
{
    x = static_cast<unsigned int>(tailleReseau/2);
    y = static_cast<unsigned int>(tailleReseau/2);

    int taille = 10;
    matrice = new QTableWidget(tailleReseau,tailleReseau);
    for(int i = 0; i < tailleReseau; i++){
        for(int j = 0; j < tailleReseau; j++){
            if (i == x && j == y)
            {
                matrice->setItem(i, j, new QTableWidgetItem(""));
                matrice->item(i, j)->setBackground(QColor(255,255,0));
                matrice->item(i, j)->setForeground(QColor(255,255,0));
            }
            else
            {
                matrice->setItem(i, j, new QTableWidgetItem(""));
                matrice->item(i, j)->setBackground(QColor(200,200,200));
                matrice->item(i, j)->setForeground(QColor(200,200,200));
            }
        }
    }
    reset();
    QHeaderView* header = matrice ->horizontalHeader();
    header->setMaximumSectionSize(15);

    header = matrice ->verticalHeader();
    header->setMaximumSectionSize(15);

    Layout = new QHBoxLayout;
    BoutonLayout = new QVBoxLayout;
    LayoutH = new QHBoxLayout;
    MatriceLabel = new QLabel("Saisissez motif");

     for (int i = 0; i<tailleReseau ; i++ ) {
        matrice->setColumnWidth(i,taille);
        matrice->setRowHeight(i,taille);
    }
    for(int i = 0; i < alphabet.size();i++){
        const QString str = QString::fromStdString(std::to_string(i)+" : "+alphabet[i].getLabel());
        QLabel* label = new QLabel(str);
        BoutonLayout->addWidget(label);
    }

    connect(matrice,SIGNAL(cellChanged(int, int)),this,SLOT(cellActivation(int, int)));

    boutonReset = new QPushButton("Reset",this);
    QObject::connect(boutonReset, SIGNAL(clicked()), this, SLOT(reset()));
    boutonValidation = new QPushButton("Valider",this);
    QObject::connect(boutonValidation, SIGNAL(clicked()), this, SLOT(accept()));
    boutonAnnulation = new QPushButton("Annuler",this);
    QObject::connect(boutonAnnulation, SIGNAL(clicked()), this, SLOT(reject()));

    BoutonLayout->addWidget(boutonAnnulation);
    BoutonLayout->addWidget(boutonReset);
    BoutonLayout->addWidget(boutonValidation);

    LayoutH->addWidget(MatriceLabel);
    LayoutH->addWidget(matrice);

    Layout->addLayout(LayoutH);
    Layout->addLayout(BoutonLayout);
    setLayout(Layout);
}

void FenetreRegleMotif::cellActivation(int row, int column)
{
    if (row != x || column != y)
    {
        int i;
        int indice = matrice->item(row,column)->text().toInt();
        bool dansVoisinage = false;
        std::vector<std::vector<int>> coor = voisinage->cordonneesVoisins();
        for(i=0;i<coor.size();i++){
            if(coor[i][0]+x ==row && coor[i][1]+y == column)
            {
                dansVoisinage = true;
                break;
            }
        }
        if((indice>=0 && indice < alphabet.size())&& dansVoisinage ){
            int r = alphabet[indice].getCouleur().getR();
            int g = alphabet[indice].getCouleur().getG();
            int b = alphabet[indice].getCouleur().getB();
            matrice->item(row, column)->setBackground(QColor(r, g, b));
            matrice->item(row, column)->setForeground(QColor(r, g, b));
            motif[i] = alphabet[indice];
        }
        else{
            matrice->item(row, column)->setText("");
        }
    }
    else
    {
        matrice->item(row, column)->setText("");
    }
}

std::vector<Etat> FenetreRegleMotif::getMotif(){
    return(motif);
}

void FenetreRegleMotif::reset(){
    std::vector<std::vector<int>> coor = voisinage->cordonneesVoisins();
    for(int i = 0; i < coor.size(); i++){
        matrice->setItem(coor[i][0]+x, coor[i][1]+y, new QTableWidgetItem("0"));
        int r = alphabet[0].getCouleur().getR();
        int g = alphabet[0].getCouleur().getG();
        int b = alphabet[0].getCouleur().getB();
        matrice->item(coor[i][0]+x, coor[i][1]+y)->setBackground(QColor(r, g, b));
        matrice->item(coor[i][0]+x, coor[i][1]+y)->setForeground(QColor(r, g, b));
        motif.push_back(alphabet[0]);
    }
}
