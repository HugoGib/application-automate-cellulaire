#include "FenetreVoisinage.h"
#include <QInputDialog>
#include <QMessageBox>
#include "FenetreRegles.h"

FenetreVoisinage::FenetreVoisinage(int tailleReseau, std::vector<Etat> alphabet, QWidget* parent):
    tailleReseau(tailleReseau),alphabet(alphabet),QWidget(parent)
{
    typeVoisinage = new QLabel("Quel type de voisinage pour votre automate ?");
    typeVoisinageComboBox = new QComboBox(this);

    typeVoisinageComboBox->addItem("Voisinage de Moore");
    typeVoisinageComboBox->addItem("Voisinage de Von Neumann");
    typeVoisinageComboBox->addItem("Voisinage arbitraire");

    boutonValider = new QPushButton("Valider",this);
    QObject::connect(boutonValider,SIGNAL(clicked()),this,SLOT(creerVoisinage()));

    layoutPrincipal = new QVBoxLayout(this);
    layoutPrincipal->addWidget(typeVoisinage);
    layoutPrincipal->addWidget(typeVoisinageComboBox);
    layoutPrincipal->addWidget(boutonValider);

    setLayout(layoutPrincipal);
}

void FenetreVoisinage::creerVoisinage()
{
    int index = typeVoisinageComboBox->currentIndex();
    switch(index)
    {
    case 0:
        voisinage = creerVoisinageMoore();
        break;
    case 1:
        voisinage = creerVoisinageNeuman();
        break;
    case 2:
        voisinage = creerVoisinageArbitraire();
        break;
    }
    if(voisinage != nullptr)
    {
        FenetreRegles* fen = new FenetreRegles(tailleReseau,alphabet,voisinage);
        fen->show();
        hide();
    }
}

Voisinage* FenetreVoisinage::creerVoisinageMoore()
{
    bool ok;
    int rayon = QInputDialog::getInt(this, "Sélection du rayon",
                                         tr("Rayon du voisinage de Moore :"), 1,
                                        1,1000,1, &ok);
    if (!ok)
    {
        QMessageBox msgBox;
        msgBox.setText("Le rayon doit être choisi.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
        return nullptr;
    }
    return new Voisinage_Moore(rayon);
}


Voisinage* FenetreVoisinage::creerVoisinageNeuman()
{
    bool ok;
    int rayon = QInputDialog::getInt(this, "Sélection du rayon",
                                         tr("Rayon du voisinage de Von Neumann :"), 1,
                                        1,1000,1, &ok);
    if (!ok)
    {
        QMessageBox msgBox;
        msgBox.setText("Le rayon doit être choisi.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
        return nullptr;
    }
    return new Voisinage_Neumann(rayon);
}


Voisinage* FenetreVoisinage::creerVoisinageArbitraire()
{
    FenetreVoisinageArbitraire* fen = new FenetreVoisinageArbitraire(tailleReseau,this);
    fen->exec();
    if(fen->Accepted)
    {
        return  new Voisinage_Arbitraire(fen->getCoordonnees());
    }
    QMessageBox msgBox;
    msgBox.setText("Le voisinage n'a pas été défini.");
    msgBox.setStandardButtons(QMessageBox::Cancel);
    msgBox.exec();
    return nullptr;
}
