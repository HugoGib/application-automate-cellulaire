/**
 * \file          FenetreRegles.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour ajouter une nouvelle regle a l'automate.
 *
 */

#ifndef FENETRE_REGLES
#define FENETRE_REGLES

#include "Etat.h"
#include "Regle.h"
#include "Voisinage.h"
#include <vector>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QPushButton>

 /**
  * \class FenetreRegles
  * \brief	Classe pour creer une fenetre afin d'ajouter une nouvelle regle a l'automate.
  *
  *
  */
class FenetreRegles : public QWidget
{
    Q_OBJECT
    QVBoxLayout* layoutMain;
    QHBoxLayout* layoutGeneral;
    QVBoxLayout* layoutAjoutRegles;
    QListWidget* listeRegles;
    QPushButton* boutonValider;
    QPushButton* boutonAjouterRegleNbVoisins;
    QPushButton* boutonAjouterRegleAuto;
    QPushButton* boutonAjouterRegleMotif;
    QPushButton* boutonMonterPriorite;
    QPushButton* boutonBaisserPriorite;
    QPushButton* boutonSupprimer;
    unsigned int tailleReseau;
    std::vector<Etat> alphabet;
    Voisinage* voisinage;
    std::vector<Regle*> regles;

public:
    explicit FenetreRegles(unsigned int tailleReseau, std::vector<Etat> alphabet, Voisinage* voisinage, QWidget* parent = nullptr);

private slots:
    void valider();
    void ajouterRegleNbVoisins();
    void ajouterRegleAuto();
    void ajouterRegleMotif();
    void monterPriorite();
    void baisserPriorite();
    void supprimerRegle();
};


#endif
