#ifndef SIMULATEUR_H
#define SIMULATEUR_H
#include"Automate.h"

/**
 * \file          Simulateur.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definition du simulateur.
 *
 */

 /**
   * \class Simulateur
   * \brief	Classe du simulateur, c'est un singleton.
   *
   *
   */

class Simulateur {
private:
    Automate* automate;
	Reseau const reseau_initial;
    Reseau* reseau;
    Simulateur(Automate* a, Reseau* r) : automate(a), reseau_initial(*r), reseau(r) {}
    /**
* \fn Simulateur(Automate* a, Reseau* r)
* \brief Constructeur de la classe Simulateur, constructeur prive car Simulateur est un singleton.
* \param a de type Automate*, pointeur sur l'automate que l'on utilise pour la simulation.
* \param r de type Reseau*, pointeur sur le reseau que l'on utilise pour la simulation.
*/
    ~Simulateur() { 
        delete automate;
        delete reseau;
    }
    static Simulateur* instance;
public:
    const Automate& getAutomate(){return *automate;};
    /**
* \fn const Automate& getAutomate()
* \brief Getter const qui retourne l'automate de la simulation.
*
* \return Objet de type Automate&
*/
    const Reseau getReseau(){return *reseau;};
    /**
* \fn const Reseau getReseau()
* \brief Getter const qui retourne le reseau de la simulation.
*
* \return Objet de type Reseau
*/
    const Reseau getReseauInitial() const { return reseau_initial; };
    /**
* \fn const Reseau getReseauInitial() const
* \brief Getter const qui retourne le reseau_intitial de la simulation.
*
* \return Objet de type Reseau
*/
	void run() {
        automate->Transition(*reseau);
	}
    /**
* \fn void run()
* \brief Fonction qui fait avancer la simulation d'un instant t a t+1
*/
    static Simulateur* getInstance(Automate* automate, Reseau* reseau);
    /**
* \fn static Simulateur* getInstance(Automate* automate, Reseau* reseau)
* \brief Getter const qui retourne l'instance de la simulation. Si l'instance est nulle, cette fonction creer une nouvelle instance.
* \param automate de type Automate*, pointeur sur l'automate que l'on utilise pour la simulation.
* \param reseau de type Reseau*, pointeur sur le reseau que l'on utilise pour la simulation.
*
* \return Objet de type Simulateur*
*/
    static Simulateur* getInstance(){return Simulateur::instance;};
    /**
* \fn const Reseau getInstance() const
* \brief Getter const qui retourne l'instance de la simulation.
*
* \return Objet de type Simulateur*
*/
    void reset();
    /**
* \fn void reset()
* \brief Fonction qui remet le reseau initial dans la simulation.
*/
};


#endif // SIMULATEUR_H
