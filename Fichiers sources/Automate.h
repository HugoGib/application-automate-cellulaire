#ifndef AUTOMATE_H
#define AUTOMATE_H
#include <string>
#include <iostream>
#include<vector>
#include"Etat.h"
#include"Reseau.h"
#include"Voisinage.h"
#include"Regle.h"

/**
 * \file          Automate.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Definit les differents automates avec leurs attributs.
 *
 */

 /**
  * \class Automate
  * \brief	Classe de base pour les automates, elle doit etre herite dans tous les automates
  * 
  *  Pour ajouter un nouvel Automate, il suffit de creer une nouvelle classe qui herite de Automate.
  *  Il faut ensuite dans le constructeur du nouvel automate, creer les regles du nouvel automate ainsi que sont alphabet.
  * ATTENTION faites bien attention a trier vos regles, l'attribut regles est une liste de regles qu'on parcours lors de la fonction de transition, s'il y'a deux regles qui s'appliquent en meme temps,
  * c'est celle qui a l'indice le plus faible dans l'attribut regles qui se declanche, l'autre ne declanchera pas !
  *
  */
class Automate {
public:
	Automate(Voisinage* voisinage) : typeVoisinage(voisinage) {};
	virtual ~Automate() {
		delete typeVoisinage;
		for (unsigned int i = 0; i < regles.size(); i++)
			delete regles[i];
	}
	/**
* \fn Automate(Voisinage* voisinage)
* \brief Constructeur de la classe Automate, ce constructeur va servir dans toutes les classes qui herite d'Automate, il creer seulement le type de voisinage que va utiliser la classe qui herite d'automate
* \param voisinage objet de type Voisinage* qui represente le voisinage de l'automate qui herite de cette classe.
*/
    void Transition(Reseau& R);
	/**
 * \fn void Transition(Reseau& R)
 * \brief Fonction qui applique les regles de l'automate au reseau qu'il prend en argument, le reseau est modifie a la fin de la fonction est represente le reseau a l'instant t+1.
 *
 * \param R Reseau pris par reference, ce dernier va etre modifier par l'automate.
 * \return Cette fonction ne retourne rien.
 */
    const std::vector<Etat> getAlphabet() const { return alphabet; };
	/**
 * \fn std::vector<Etat> getAlphabet() const
 * \brief Getter const de l'alphabet lie a l'automate qui contient tous les etats possible dans cet automate.
 *
 * \return objet de type std::vector<Etat>.
 */
    std::vector<Regle*> getRegles() const { return regles; };
	/**
* \fn std::vector<Regle*> getRegles() const
* \brief Getter const qui retourne les regles de transition de l'automate.
*
* \return Objet de type std::vector<Regle*>
*/
    virtual Reseau initialisation(unsigned int l, unsigned int h);
	/**
* \fn virtual Reseau initialisation(unsigned int l, unsigned int h)
* \brief Fonction virtuelle initialisant un reseau aleatoire avec l'alphabet de l'automate. Cette fonction peut etre redefinie dans les classes heritees si elle doivent avoir un reseau de depart specifique.
*
* \param l unsigned int qui represente la largeur du reseau que l'on va creer.
* \param h unsigned int qui represente la hauteur du reseau que l'on va creer.
* \return Objet de type Reseau
*/
protected:
	Voisinage* typeVoisinage;
	std::vector<Regle*> regles;
	std::vector<Etat> alphabet; /*Ici on repertorie les differents etats possibles TRIE par INDICE
							 c'est important pour la fonction de transition*/
};

/**
  * \class Automate_Life_Game
  * \brief	Classe de l'automate Life Game
  */
class Automate_Life_Game : public Automate {
public:
	Automate_Life_Game();
	/**
* \fn Automate_Life_Game()
* \brief Constructeur de l'automate Life Game, ce constructeur creer les regles et l'alphabet de l'automate Life Game.
*
*/
private:
};

/**
  * \class Automate_Brian_Brain
  * \brief	Classe de l'automate Brian's brain.
  */
class Automate_Brian_Brain : public Automate {
public:
	Automate_Brian_Brain();
	/**
* \fn Automate_Brian_Brain()
* \brief Constructeur de l'automate Brain's brain, ce constructeur creer les regles et l'alphabet de l'automate Brian's brain.
*
*/
private:
};

/**
  * \class Automate_Griffeath
  * \brief	Classe de l'automate de Griffeath.
  */
class Automate_Griffeath : public Automate {
public:
	Automate_Griffeath();
	/**
* \fn Automate_Griffeath()
* \brief Constructeur de l'automate Griffeath, ce constructeur creer les regles et l'alphabet de l'automate Griffeath
*
*/
private:
};

/**
  * \class Automate_Langton_Loop
  * \brief	Classe de l'automate boucle de Langton
  */
class Automate_Langton_Loop : public Automate {
public:
	Automate_Langton_Loop();
	/**
* \fn Automate_Langton_Loop()
* \brief Constructeur de l'automate Boucle de Langton, ce constructeur creer les regles et l'alphabet de l'automate Boucle de Langton
*
*/
    Reseau initialisation(unsigned int l, unsigned int h);
	/**
* \fn Reseau initialisation(unsigned int l, unsigned int h)
* \brief Surcharge de la fonction initialisation d'Automate. Fonction initialisant un reseau avec qui permet d'avoir la configuration de base de la boucle de Langton au centre du reseau.
*
* \param l unsigned int qui represente la largeur du reseau que l'on va creer.
* \param h unsigned int qui represente la hauteur du reseau que l'on va creer.
* \return Objet de type Reseau
*/
private:
};

/**
  * \class Automate_Utilisateur
  * \brief	Classe de l'automate utilisateur
  */
class Automate_Utilisateur : public Automate {
public:
	Automate_Utilisateur(std::vector<Etat> Alphabet, std::vector<Regle*> Regles, Voisinage* voisinage) : Automate(voisinage) {
		alphabet = Alphabet;
		regles = Regles;
	};
	/**
* \fn Automate_Utilisateur(std::vector<Etat> Alphabet, std::vector<Regle*> Regles, Voisinage* voisinage) : Automate(voisinage)
* \brief Constructeur de l'automate Utilisateur, ce constructeur creer les regles et l'alphabet de l'automate Utilisateur a partir de ces parametres
* \param Alphabet objet de type std::vector<Etat> qui represente l'alphabet de l'automate utilisateur.
* \param Regles objet de type std::vector<Regle*> qui represente la liste de regles de l'automate utilisateur.
* \param voisinage objet de type Voisinage* qui represente le voisinage de l'automate utilisateur.
*/
private:
};

/**
  * \class Automate_Fourmi_Langton
  * \brief	Classe de l'automate de la fourmi de Langton
  */
class Automate_Fourmi_Langton : public Automate {
public:
    Automate_Fourmi_Langton();
    Reseau initialisation(unsigned int l, unsigned int h);
    /**
* \fn Automate_Fourmi_Langton()
* \brief Constructeur de l'automate fourmi de Langton, ce constructeur cree les regles et l'alphabet de l'automate.
*
*/
private:
};

#endif // !AUTOMATE_H
