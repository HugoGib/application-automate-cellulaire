#include"Voisinage.h"
/*VOISINAGE MOORE*/

std::vector<Etat> Voisinage_Moore::voisins(const Reseau& R ,int i, int j) {
	std::vector<Etat> voisinage;
	int hauteur = R.getHauteur();
	int largeur = R.getLargeur();
	int a, b, A, B; // ce sont des index !
	for (a = -rayon; a <= rayon; ++a) {
		for (b = -rayon; b <= rayon; ++b) {
			if (a != 0 || b != 0) {
				A = (i + a + largeur) % largeur; // A est l'abscisse absolue du voisin courant
				// si i+a est n�gatif, le +largeur le remet dans le range [0,largeur-1]
				// si i+a > largeur, le %largeur le remet dans le range [0,largeur-1]
				B = (j + b + hauteur) % hauteur; // B est l'ordonn�e absolue du voisin courant
				voisinage.push_back(R.getCellule(A, B));
			}
		}
	}
	return(voisinage);
}
	
std::vector<std::vector<int>> Voisinage_Moore::cordonneesVoisins(){
    std::vector<std::vector<int>> coordonnees;
    int a, b; // ce sont des index !
    for (a = -rayon; a <= rayon; ++a) {
        for (b = -rayon; b <= rayon; ++b) {
            if (a != 0 || b != 0) {
                coordonnees.push_back({a,b});
            }
        }
    }
    return coordonnees;
}

/*VOISINAGE NEUMANN*/
/*Dans ce voisinage, on ne prend pas en compte les cellules voisines sur les diagonales autour de la cellule concern�
par le voisinage*/

std::vector<Etat> Voisinage_Neumann::voisins(const Reseau& R, int i, int j) {
	std::vector<Etat> voisinage;
	int hauteur = R.getHauteur();
	int largeur = R.getLargeur();
    int a, b, A, B; // ce sont des index !
    for (a = -rayon; a <= rayon; ++a) {
        for (b = -rayon; b <= rayon; ++b) {
            if ((a != 0 || b != 0) && abs(a)+abs(b) <= rayon) {
                A = (i + a + largeur) % largeur; // A est l'abscisse absolue du voisin courant
                // si i+a est n�gatif, le +largeur le remet dans le range [0,largeur-1]
                // si i+a > largeur, le %largeur le remet dans le range [0,largeur-1]
                B = (j + b + hauteur) % hauteur; // B est l'ordonn�e absolue du voisin courant
                voisinage.push_back(R.getCellule(A, B));
            }
        }
    }
    return(voisinage);
}

std::vector<std::vector<int>> Voisinage_Neumann::cordonneesVoisins(){
    std::vector<std::vector<int>> coordonnees;
    int a, b; // ce sont des index !
    for (a = -rayon; a <= rayon; ++a) {
        for (b = -rayon; b <= rayon; ++b) {
            if (a != 0 || b != 0) {
                if(abs(a)+abs(b) <= rayon)
                    coordonnees.push_back({a,b});
            }
        }
    }
    return coordonnees;
}


/*VOISINAGE ARBITRAIRE*/

std::vector<Etat> Voisinage_Arbitraire::voisins(const Reseau& R, int i, int j)
{
    std::vector<Etat> resultat = {};
    for(int k = 0; k < coordonnees.size(); k++){
        resultat.push_back(R.getCellule((coordonnees[k][0]+i+R.getHauteur())%R.getHauteur(),(coordonnees[k][1]+j+R.getLargeur())%R.getLargeur()));
    }
    return resultat;
}

std::vector<std::vector<int>> Voisinage_Arbitraire::cordonneesVoisins(){
    return coordonnees;
}

/*VOISINAGE LANGTON LOOP*/

std::vector<Etat> Voisinage_Langton_Loop::voisins(const Reseau& R, int i, int j) {
    std::vector<Etat> voisinage;
    int hauteur = R.getHauteur();
    int largeur = R.getLargeur();
    int a, A, B; // ce sont des index !
    for (a = -1; a <= 1; ++a) {
        A = (i - a + largeur) % largeur; // A est l'abscisse absolue du voisin courant
        B = (j + a + hauteur) % hauteur; // B est l'ordonn�e absolue du voisin courant
        if (A != i)
            voisinage.push_back(R.getCellule(A, j));
        if (B != j)
            voisinage.push_back(R.getCellule(i, B));
    }
    return(voisinage);
}

std::vector<std::vector<int>> Voisinage_Langton_Loop::cordonneesVoisins(){
    std::vector<std::vector<int>> coordonnees = {{-1,0}, {0,1}, {1,0},{0,-1}};
    return coordonnees;
}
