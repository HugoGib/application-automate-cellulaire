/**
 * \file          FenetreSimulation.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour afficher une simulation.
 *
 */
#ifndef FENETRESIMULATION_H
#define FENETRESIMULATION_H
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QModelIndex>
#include <QTableWidget>
#include <QHeaderView>
#include <QTimer>
#include <QSlider>
#include "Simulateur.h"

 /**
  * \class FenetreSimulation
  * \brief	Classe pour creer une fenetre une fenetre pour afficher une simulation.
  *
  *
  */
class FenetreSimulation : public QWidget
{

    Q_OBJECT
    QVBoxLayout* ResLayout;
    QVBoxLayout* LabelLayout;
    QLabel* ResLabel;
    QHBoxLayout* LayoutH;
    QPushButton* boutonReseau;
    QPushButton* boutonRun;
    QPushButton* boutonReset;
    QTableWidget* res;
    QVBoxLayout* VitesseLayout;
    QLabel* labelVitesse;
    QLabel* labelIteration;
    QSlider* sliderVitesse;
    QTimer* timer;
    bool simulationEnCours;
    int iteration;
    unsigned int sauteIteration; //vaut 0 si on ne saute pas d'iteration, le nombre d'iteration sinon

public:
    explicit FenetreSimulation(Simulateur* sim, QWidget* parent = nullptr);
public:
    Simulateur* simulateur;
private slots:
    void runSim();
    void runSimAuto();
    void changeSimulationState();
    void changerVitesse(int nouvell_vitesse);
    void reset();
    void actualiserTable();
};

#endif // FENETRESIMULATION_H
