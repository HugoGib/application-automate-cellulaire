/**
 * \file          FenetreReseau.h
 * \author    Hugo Gibert
 * \version   1.0
 * \brief       Creer une fenetre pour creer un reseau initial.
 *
 */
#ifndef FENETRERESEAU_H
#define FENETRERESEAU_H
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QModelIndex>
#include <QTableWidget>
#include <QHeaderView>
#include "Automate.h"
#include "Reseau.h"
#include "Simulateur.h"
#include "FenetreSimulation.h"


 /**
  * \class FenetreReseau
  * \brief	Classe pour creer une fenetre pour creer un reseau initial.
  *
  *
  */
class FenetreReseau : public QWidget
{

    Q_OBJECT
    QVBoxLayout* ResLayout;
    QVBoxLayout* LabelLayout;
    QLabel* ResLabel;
    QHBoxLayout* LayoutH;
    QPushButton* boutonReseau;
    QTableWidget* res;
    QPushButton* boutonInitialisation;
    QPushButton* boutonReset;
    unsigned int tailleReseau;

public:
    explicit FenetreReseau(Automate* A, unsigned int tailleReseau, QWidget* parent = nullptr);
    Automate* automate;
    Reseau* reseau;
private slots:
    void createSimulation();
    void cellActivation(int row, int column);
    void initialisation();
    void reset();
};


#endif // FENETRERESEAU_H
