#include "FenetreAlphabet.h"
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>
#include "FenetreVoisinage.h"

FenetreAlphabet::FenetreAlphabet(unsigned int tailleReseau, QWidget* parent):tailleReseau(tailleReseau),QWidget(parent),alphabet(0)
{

    listeEtats = new QListWidget(this);

    boutonAjouter = new QPushButton("Ajouter état",this);
    QObject::connect(boutonAjouter, SIGNAL(clicked()), this, SLOT(ajouterEtat()));

    boutonSupprimer = new QPushButton("Supprimer état",this);
    QObject::connect(boutonSupprimer, SIGNAL(clicked()), this, SLOT(supprimerEtat()));

    layoutAjoutEtat = new QHBoxLayout();
    layoutAjoutEtat->addWidget(boutonAjouter);
    layoutAjoutEtat->addWidget(boutonSupprimer);

    boutonValider = new QPushButton("Valider",this);
    QObject::connect(boutonValider, SIGNAL(clicked()), this, SLOT(valider()));

    layoutGeneral = new QVBoxLayout();
    layoutGeneral->addWidget(listeEtats);
    layoutGeneral->addLayout(layoutAjoutEtat);
    layoutGeneral->addWidget(boutonValider);

    setLayout(layoutGeneral);
}

void FenetreAlphabet::valider(){
    if(alphabet.size() == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("L'alphabet est vide.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
    }
    else
    {
        FenetreVoisinage* fenetreVoisin = new FenetreVoisinage(tailleReseau,alphabet);
        fenetreVoisin->show();
        hide();
    }
}


void FenetreAlphabet::ajouterEtat(){
    bool ok;
    QString label = QInputDialog::getText(this, "Création d'un état",
                                         tr("Label :"), QLineEdit::Normal,
                                        "", &ok);
    if (!ok || label.isEmpty())
    {
        QMessageBox msgBox;
        msgBox.setText("Le label est invalide.");
        msgBox.setStandardButtons(QMessageBox::Cancel);
        msgBox.exec();
    }
    else
    {
        int rouge = QInputDialog::getInt(this, "Sélection de la couleur de l'état",
                                             tr("Composante rouge (entre 0 et 255) :"), 0,
                                            0,255,1, &ok);
        if (!ok)
        {
            QMessageBox msgBox;
            msgBox.setText("Le nombre doit être en 0 et 255.");
            msgBox.setStandardButtons(QMessageBox::Cancel);
            msgBox.exec();
        }
        else
        {
            int vert = QInputDialog::getInt(this, "Sélection de la couleur de l'état",
                                                 tr("Composante verte (entre 0 et 255) :"), 0,
                                                0,255,1, &ok);
            if (!ok)
            {
                QMessageBox msgBox;
                msgBox.setText("Le nombre doit être en 0 et 255.");
                msgBox.setStandardButtons(QMessageBox::Cancel);
                msgBox.exec();
            }
            else{
                int bleu = QInputDialog::getInt(this, "Sélection de la couleur de l'état",
                                                     tr("Composante bleu (entre 0 et 255) :"), 0,
                                                    0,255,1, &ok);
                if (!ok)
                {
                    QMessageBox msgBox;
                    msgBox.setText("Le nombre doit être en 0 et 255.");
                    msgBox.setStandardButtons(QMessageBox::Cancel);
                    msgBox.exec();
                }
                else{
                    alphabet.push_back(Etat(rouge,vert,bleu,listeEtats->count(),label.toStdString()));
                    listeEtats->addItem(label);
                }
            }
        }
    }
}

void FenetreAlphabet::supprimerEtat(){
    QList liste_eleme_a_suppr = listeEtats->selectedItems();
    for(int i = 0; i < liste_eleme_a_suppr.count(); i++)
    {
        listeEtats->takeItem(listeEtats->row(liste_eleme_a_suppr[i]));
        alphabet[i] = alphabet[alphabet.size()-1];
        alphabet.pop_back();
    }
}
